#!/bin/bash

#
# Copyright (c) OPEKEPE 2021 -- 2021. All rights reserved
#
# Project and code is made available under the EU-PL license.
#

/bin/chown -R postgres:postgres /niva/niva_blob
/bin/chown -R postgres:postgres /niva/niva_DATA
