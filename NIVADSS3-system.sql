-- Permissions' Management Database

-- NEUROPUBLIC / MT
-- v20220525a

/**
*
* © 2022 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/



-- jndi-name="java:/niva_usermgmnt"
begin transaction;

--add module group with only this new application
INSERT INTO subscription.ss_module_groups        
	(mogr_name)
	SELECT 'ALL MODULES'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_module_groups WHERE mogr_name='ALL MODULES');


-- add application
INSERT INTO subscription.ss_modules 
	(modu_name, modu_description, modu_isfree, modu_ulr_suffix) 
	SELECT 'Niva', 'NIVA - Use Case 1a: Decision Support System V3', false, 'Niva'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_modules WHERE modu_name='Niva');

UPDATE subscription.ss_modules
   SET modu_description='NIVA - Use Case 1a: Decision Support System V3'
   WHERE modu_name = 'Niva';


-- associate application with module
INSERT INTO subscription.ss_grouping_modules 
    (mogr_id, modu_id) 
    SELECT
        (SELECT mogr_id FROM subscription.ss_module_groups WHERE mogr_name='ALL MODULES'), 
        (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    WHERE NOT EXISTS (
    SELECT * FROM subscription.ss_grouping_modules 
        WHERE 
              mogr_id in (SELECT mogr_id FROM subscription.ss_module_groups WHERE mogr_name='ALL MODULES')
        AND   modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));
-- add superadmin system role
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id, syro_is_multipage_role)
	SELECT 'Niva_superadmin', 'Ρόλος SuperAdmin για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'), true
	WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles WHERE syro_name='Niva_superadmin'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));
-- add admin system role
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id, syro_is_multipage_role)
	SELECT 'Niva_admin', 'Ρόλος Διαχειριστή για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'), true
	WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles WHERE syro_name='Niva_admin'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));
-- add guest system role
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id, syro_is_multipage_role)
	SELECT 'Niva_guest', 'Ρόλος Επισκέπτη για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'), true
	WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles WHERE syro_name='Niva_guest'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- delete any previously assigned Security Classes in any privilege of module 'Niva'
delete from subscription.ss_system_privileges_security_classes a
 where a.sypr_id in (select b.sypr_id 
                       from subscription.ss_system_privileges b
                       	join subscription.ss_modules c on c.modu_id = b.modu_id
                      where c.modu_name = 'Niva');

INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id, syro_is_multipage_role)
	SELECT 'Niva_superadmin', 'Ρόλος SuperAdmin για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'), true
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_superadmin'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));
-- delete any privilege under this system role
delete from subscription.ss_system_roles_privileges syrp
 where syro_id in (SELECT syro_id FROM subscription.ss_system_roles where syro_name='Niva_superadmin' and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id, syro_is_multipage_role)
	SELECT 'Niva_admin', 'Ρόλος Διαχειριστή για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'), true
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_admin'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));
-- delete any privilege under this system role
delete from subscription.ss_system_roles_privileges syrp
 where syro_id in (SELECT syro_id FROM subscription.ss_system_roles where syro_name='Niva_admin' and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id, syro_is_multipage_role)
	SELECT 'Niva_guest', 'Ρόλος Επισκέπτη για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'), true
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_guest'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));
-- delete any privilege under this system role
delete from subscription.ss_system_roles_privileges syrp
 where syro_id in (SELECT syro_id FROM subscription.ss_system_roles where syro_name='Niva_guest' and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id, syro_is_multipage_role)
	SELECT 'Niva_WebServices', 'Niva: Web Services Call permission.',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'), true
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_WebServices'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));
-- delete any privilege under this system role
delete from subscription.ss_system_roles_privileges syrp
 where syro_id in (SELECT syro_id FROM subscription.ss_system_roles where syro_name='Niva_WebServices' and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- Add privilege Niva_ParcelsImport_R

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_ParcelsImport_R'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_ParcelsImport_R');

-- Add a system role for privilege 'Niva_ParcelsImport_R'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_ParcelsImport_R', 'Δικαίωμα ανάγνωσης στην οθόνη Import of Classification n Declaration Data για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_ParcelsImport_R'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα ανάγνωσης στην οθόνη Import of Classification n Declaration Data για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_ParcelsImport_R'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα ανάγνωσης στην οθόνη Import of Classification n Declaration Data για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_ParcelsImport_R');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_ParcelsImport_R'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_ParcelsImport_R') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_ParcelsImport_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsImport_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_ParcelsImport_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsImport_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsImport_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsImport_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsImport_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsImport_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the guest system role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
    (syro_id, sypr_id, modu_id)
    SELECT (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
           (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsImport_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
           (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
     WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
                        WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                          and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsImport_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                       );

-- Add privilege Niva_ParcelsImport_W

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_ParcelsImport_W'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_ParcelsImport_W');

-- Add a system role for privilege 'Niva_ParcelsImport_W'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_ParcelsImport_W', 'Δικαίωμα εγγραφής στην οθόνη Import of Classification n Declaration Data για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_ParcelsImport_W'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα εγγραφής στην οθόνη Import of Classification n Declaration Data για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_ParcelsImport_W'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα εγγραφής στην οθόνη Import of Classification n Declaration Data για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_ParcelsImport_W');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_ParcelsImport_W'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_ParcelsImport_W') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_ParcelsImport_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsImport_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_ParcelsImport_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsImport_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsImport_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsImport_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsImport_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsImport_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_ParcelsImport_D

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_ParcelsImport_D'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_ParcelsImport_D');

-- Add a system role for privilege 'Niva_ParcelsImport_D'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_ParcelsImport_D', 'Δικαίωμα διαγραφής στην οθόνη Import of Classification n Declaration Data για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_ParcelsImport_D'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα διαγραφής στην οθόνη Import of Classification n Declaration Data για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_ParcelsImport_D'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα διαγραφής στην οθόνη Import of Classification n Declaration Data για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_ParcelsImport_D');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_ParcelsImport_D'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_ParcelsImport_D') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_ParcelsImport_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsImport_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_ParcelsImport_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsImport_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsImport_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsImport_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsImport_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsImport_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_Classifier_R

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Classifier_R'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Classifier_R');

-- Add a system role for privilege 'Niva_Classifier_R'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Classifier_R', 'Δικαίωμα ανάγνωσης στην οθόνη Classifier για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Classifier_R'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα ανάγνωσης στην οθόνη Classifier για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Classifier_R'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα ανάγνωσης στην οθόνη Classifier για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Classifier_R');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Classifier_R'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Classifier_R') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Classifier_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Classifier_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the guest system role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
    (syro_id, sypr_id, modu_id)
    SELECT (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
           (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
           (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
     WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
                        WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                          and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                       );

-- Add privilege Niva_Classifier_W

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Classifier_W'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Classifier_W');

-- Add a system role for privilege 'Niva_Classifier_W'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Classifier_W', 'Δικαίωμα εγγραφής στην οθόνη Classifier για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Classifier_W'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα εγγραφής στην οθόνη Classifier για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Classifier_W'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα εγγραφής στην οθόνη Classifier για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Classifier_W');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Classifier_W'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Classifier_W') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Classifier_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Classifier_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_Classifier_D

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Classifier_D'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Classifier_D');

-- Add a system role for privilege 'Niva_Classifier_D'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Classifier_D', 'Δικαίωμα διαγραφής στην οθόνη Classifier για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Classifier_D'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα διαγραφής στην οθόνη Classifier για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Classifier_D'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα διαγραφής στην οθόνη Classifier για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Classifier_D');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Classifier_D'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Classifier_D') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Classifier_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Classifier_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_CoverType_R

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_CoverType_R'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_CoverType_R');

-- Add a system role for privilege 'Niva_CoverType_R'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_CoverType_R', 'Δικαίωμα ανάγνωσης στην οθόνη Cover Types για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_CoverType_R'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα ανάγνωσης στην οθόνη Cover Types για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_CoverType_R'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα ανάγνωσης στην οθόνη Cover Types για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_CoverType_R');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_CoverType_R'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_CoverType_R') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_CoverType_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_CoverType_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the guest system role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
    (syro_id, sypr_id, modu_id)
    SELECT (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
           (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
           (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
     WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
                        WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                          and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                       );

-- Add privilege Niva_CoverType_W

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_CoverType_W'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_CoverType_W');

-- Add a system role for privilege 'Niva_CoverType_W'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_CoverType_W', 'Δικαίωμα εγγραφής στην οθόνη Cover Types για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_CoverType_W'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα εγγραφής στην οθόνη Cover Types για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_CoverType_W'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα εγγραφής στην οθόνη Cover Types για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_CoverType_W');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_CoverType_W'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_CoverType_W') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_CoverType_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_CoverType_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_CoverType_D

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_CoverType_D'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_CoverType_D');

-- Add a system role for privilege 'Niva_CoverType_D'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_CoverType_D', 'Δικαίωμα διαγραφής στην οθόνη Cover Types για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_CoverType_D'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα διαγραφής στην οθόνη Cover Types για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_CoverType_D'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα διαγραφής στην οθόνη Cover Types για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_CoverType_D');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_CoverType_D'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_CoverType_D') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_CoverType_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_CoverType_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_Cultivation_R

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Cultivation_R'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Cultivation_R');

-- Add a system role for privilege 'Niva_Cultivation_R'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Cultivation_R', 'Δικαίωμα ανάγνωσης στην οθόνη Crops για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Cultivation_R'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα ανάγνωσης στην οθόνη Crops για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Cultivation_R'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα ανάγνωσης στην οθόνη Crops για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Cultivation_R');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Cultivation_R'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Cultivation_R') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Cultivation_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Cultivation_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the guest system role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
    (syro_id, sypr_id, modu_id)
    SELECT (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
           (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
           (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
     WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
                        WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                          and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                       );

-- Add privilege Niva_Cultivation_W

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Cultivation_W'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Cultivation_W');

-- Add a system role for privilege 'Niva_Cultivation_W'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Cultivation_W', 'Δικαίωμα εγγραφής στην οθόνη Crops για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Cultivation_W'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα εγγραφής στην οθόνη Crops για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Cultivation_W'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα εγγραφής στην οθόνη Crops για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Cultivation_W');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Cultivation_W'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Cultivation_W') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Cultivation_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Cultivation_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_Cultivation_D

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Cultivation_D'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Cultivation_D');

-- Add a system role for privilege 'Niva_Cultivation_D'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Cultivation_D', 'Δικαίωμα διαγραφής στην οθόνη Crops για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Cultivation_D'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα διαγραφής στην οθόνη Crops για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Cultivation_D'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα διαγραφής στην οθόνη Crops για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Cultivation_D');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Cultivation_D'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Cultivation_D') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Cultivation_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Cultivation_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_Document_R

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Document_R'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Document_R');

-- Add a system role for privilege 'Niva_Document_R'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Document_R', 'Δικαίωμα ανάγνωσης στην οθόνη Documentation για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Document_R'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα ανάγνωσης στην οθόνη Documentation για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Document_R'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα ανάγνωσης στην οθόνη Documentation για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Document_R');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Document_R'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Document_R') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Document_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Document_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the guest system role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
    (syro_id, sypr_id, modu_id)
    SELECT (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
           (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
           (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
     WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
                        WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                          and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                       );

-- Add privilege Niva_Document_W

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Document_W'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Document_W');

-- Add a system role for privilege 'Niva_Document_W'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Document_W', 'Δικαίωμα εγγραφής στην οθόνη Documentation για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Document_W'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα εγγραφής στην οθόνη Documentation για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Document_W'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα εγγραφής στην οθόνη Documentation για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Document_W');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Document_W'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Document_W') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Document_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Document_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_Document_D

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Document_D'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Document_D');

-- Add a system role for privilege 'Niva_Document_D'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Document_D', 'Δικαίωμα διαγραφής στην οθόνη Documentation για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Document_D'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα διαγραφής στην οθόνη Documentation για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Document_D'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα διαγραφής στην οθόνη Documentation για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Document_D');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Document_D'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Document_D') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Document_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Document_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_FileTemplate_R

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_FileTemplate_R'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_FileTemplate_R');

-- Add a system role for privilege 'Niva_FileTemplate_R'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_FileTemplate_R', 'Δικαίωμα ανάγνωσης στην οθόνη Data Import Template για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_FileTemplate_R'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα ανάγνωσης στην οθόνη Data Import Template για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_FileTemplate_R'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα ανάγνωσης στην οθόνη Data Import Template για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_FileTemplate_R');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_FileTemplate_R'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_FileTemplate_R') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_FileTemplate_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_FileTemplate_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the guest system role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
    (syro_id, sypr_id, modu_id)
    SELECT (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
           (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
           (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
     WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
                        WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                          and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                       );

-- Add privilege Niva_FileTemplate_W

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_FileTemplate_W'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_FileTemplate_W');

-- Add a system role for privilege 'Niva_FileTemplate_W'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_FileTemplate_W', 'Δικαίωμα εγγραφής στην οθόνη Data Import Template για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_FileTemplate_W'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα εγγραφής στην οθόνη Data Import Template για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_FileTemplate_W'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα εγγραφής στην οθόνη Data Import Template για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_FileTemplate_W');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_FileTemplate_W'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_FileTemplate_W') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_FileTemplate_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_FileTemplate_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_FileTemplate_D

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_FileTemplate_D'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_FileTemplate_D');

-- Add a system role for privilege 'Niva_FileTemplate_D'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_FileTemplate_D', 'Δικαίωμα διαγραφής στην οθόνη Data Import Template για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_FileTemplate_D'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα διαγραφής στην οθόνη Data Import Template για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_FileTemplate_D'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα διαγραφής στην οθόνη Data Import Template για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_FileTemplate_D');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_FileTemplate_D'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_FileTemplate_D') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_FileTemplate_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_FileTemplate_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_SystemConfiguration_R

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_SystemConfiguration_R'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_SystemConfiguration_R');

-- Add a system role for privilege 'Niva_SystemConfiguration_R'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_SystemConfiguration_R', 'Δικαίωμα ανάγνωσης στην οθόνη System Configurations για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_SystemConfiguration_R'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα ανάγνωσης στην οθόνη System Configurations για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_SystemConfiguration_R'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα ανάγνωσης στην οθόνη System Configurations για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_SystemConfiguration_R');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_SystemConfiguration_R'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_SystemConfiguration_R') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_SystemConfiguration_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_SystemConfiguration_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_SystemConfiguration_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_SystemConfiguration_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_SystemConfiguration_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_SystemConfiguration_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_SystemConfiguration_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_SystemConfiguration_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the guest system role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
    (syro_id, sypr_id, modu_id)
    SELECT (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
           (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_SystemConfiguration_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
           (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
     WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
                        WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                          and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_SystemConfiguration_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                       );

-- Add privilege Niva_SystemConfiguration_W

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_SystemConfiguration_W'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_SystemConfiguration_W');

-- Add a system role for privilege 'Niva_SystemConfiguration_W'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_SystemConfiguration_W', 'Δικαίωμα εγγραφής στην οθόνη System Configurations για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_SystemConfiguration_W'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα εγγραφής στην οθόνη System Configurations για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_SystemConfiguration_W'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα εγγραφής στην οθόνη System Configurations για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_SystemConfiguration_W');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_SystemConfiguration_W'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_SystemConfiguration_W') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_SystemConfiguration_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_SystemConfiguration_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_SystemConfiguration_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_SystemConfiguration_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_SystemConfiguration_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_SystemConfiguration_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_SystemConfiguration_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_SystemConfiguration_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_SystemConfiguration_D

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_SystemConfiguration_D'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_SystemConfiguration_D');

-- Add a system role for privilege 'Niva_SystemConfiguration_D'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_SystemConfiguration_D', 'Δικαίωμα διαγραφής στην οθόνη System Configurations για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_SystemConfiguration_D'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα διαγραφής στην οθόνη System Configurations για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_SystemConfiguration_D'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα διαγραφής στην οθόνη System Configurations για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_SystemConfiguration_D');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_SystemConfiguration_D'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_SystemConfiguration_D') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_SystemConfiguration_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_SystemConfiguration_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_SystemConfiguration_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_SystemConfiguration_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_SystemConfiguration_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_SystemConfiguration_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_SystemConfiguration_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_SystemConfiguration_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_UserNotification_R

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_UserNotification_R'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_UserNotification_R');

-- Add a system role for privilege 'Niva_UserNotification_R'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_UserNotification_R', 'Δικαίωμα ανάγνωσης στην οθόνη UserNotification για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_UserNotification_R'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα ανάγνωσης στην οθόνη UserNotification για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_UserNotification_R'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα ανάγνωσης στην οθόνη UserNotification για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_UserNotification_R');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_UserNotification_R'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_UserNotification_R') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_UserNotification_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_UserNotification_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_UserNotification_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_UserNotification_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_UserNotification_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_UserNotification_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_UserNotification_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_UserNotification_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the guest system role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
    (syro_id, sypr_id, modu_id)
    SELECT (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
           (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_UserNotification_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
           (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
     WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
                        WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                          and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_UserNotification_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                       );

-- Add privilege Niva_UserNotification_W

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_UserNotification_W'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_UserNotification_W');

-- Add a system role for privilege 'Niva_UserNotification_W'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_UserNotification_W', 'Δικαίωμα εγγραφής στην οθόνη UserNotification για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_UserNotification_W'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα εγγραφής στην οθόνη UserNotification για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_UserNotification_W'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα εγγραφής στην οθόνη UserNotification για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_UserNotification_W');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_UserNotification_W'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_UserNotification_W') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_UserNotification_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_UserNotification_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_UserNotification_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_UserNotification_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_UserNotification_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_UserNotification_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_UserNotification_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_UserNotification_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_UserNotification_D

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_UserNotification_D'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_UserNotification_D');

-- Add a system role for privilege 'Niva_UserNotification_D'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_UserNotification_D', 'Δικαίωμα διαγραφής στην οθόνη UserNotification για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_UserNotification_D'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα διαγραφής στην οθόνη UserNotification για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_UserNotification_D'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα διαγραφής στην οθόνη UserNotification για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_UserNotification_D');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_UserNotification_D'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_UserNotification_D') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_UserNotification_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_UserNotification_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_UserNotification_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_UserNotification_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_UserNotification_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_UserNotification_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_UserNotification_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_UserNotification_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_EcSet_R

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_EcSet_R'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_EcSet_R');

-- Add a system role for privilege 'Niva_EcSet_R'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_EcSet_R', 'Δικαίωμα ανάγνωσης στην οθόνη Eligibility Criteria Sets για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_EcSet_R'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα ανάγνωσης στην οθόνη Eligibility Criteria Sets για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_EcSet_R'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα ανάγνωσης στην οθόνη Eligibility Criteria Sets για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_EcSet_R');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_EcSet_R'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_EcSet_R') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_EcSet_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcSet_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_EcSet_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcSet_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcSet_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcSet_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcSet_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcSet_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the guest system role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
    (syro_id, sypr_id, modu_id)
    SELECT (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
           (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcSet_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
           (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
     WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
                        WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                          and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcSet_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                       );

-- Add privilege Niva_EcSet_W

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_EcSet_W'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_EcSet_W');

-- Add a system role for privilege 'Niva_EcSet_W'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_EcSet_W', 'Δικαίωμα εγγραφής στην οθόνη Eligibility Criteria Sets για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_EcSet_W'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα εγγραφής στην οθόνη Eligibility Criteria Sets για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_EcSet_W'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα εγγραφής στην οθόνη Eligibility Criteria Sets για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_EcSet_W');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_EcSet_W'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_EcSet_W') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_EcSet_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcSet_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_EcSet_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcSet_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcSet_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcSet_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcSet_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcSet_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_EcSet_D

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_EcSet_D'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_EcSet_D');

-- Add a system role for privilege 'Niva_EcSet_D'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_EcSet_D', 'Δικαίωμα διαγραφής στην οθόνη Eligibility Criteria Sets για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_EcSet_D'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα διαγραφής στην οθόνη Eligibility Criteria Sets για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_EcSet_D'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα διαγραφής στην οθόνη Eligibility Criteria Sets για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_EcSet_D');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_EcSet_D'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_EcSet_D') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_EcSet_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcSet_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_EcSet_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcSet_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcSet_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcSet_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcSet_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcSet_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_Paymentsscheme_R

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Paymentsscheme_R'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Paymentsscheme_R');

-- Add a system role for privilege 'Niva_Paymentsscheme_R'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Paymentsscheme_R', 'Δικαίωμα ανάγνωσης στην οθόνη Manage Aid Schemes για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Paymentsscheme_R'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα ανάγνωσης στην οθόνη Manage Aid Schemes για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Paymentsscheme_R'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα ανάγνωσης στην οθόνη Manage Aid Schemes για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Paymentsscheme_R');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Paymentsscheme_R'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Paymentsscheme_R') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Paymentsscheme_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Paymentsscheme_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Paymentsscheme_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Paymentsscheme_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Paymentsscheme_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Paymentsscheme_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Paymentsscheme_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Paymentsscheme_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the guest system role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
    (syro_id, sypr_id, modu_id)
    SELECT (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
           (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Paymentsscheme_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
           (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
     WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
                        WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                          and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Paymentsscheme_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                       );

-- Add privilege Niva_Paymentsscheme_W

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Paymentsscheme_W'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Paymentsscheme_W');

-- Add a system role for privilege 'Niva_Paymentsscheme_W'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Paymentsscheme_W', 'Δικαίωμα εγγραφής στην οθόνη Manage Aid Schemes για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Paymentsscheme_W'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα εγγραφής στην οθόνη Manage Aid Schemes για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Paymentsscheme_W'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα εγγραφής στην οθόνη Manage Aid Schemes για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Paymentsscheme_W');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Paymentsscheme_W'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Paymentsscheme_W') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Paymentsscheme_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Paymentsscheme_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Paymentsscheme_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Paymentsscheme_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Paymentsscheme_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Paymentsscheme_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Paymentsscheme_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Paymentsscheme_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_Paymentsscheme_D

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Paymentsscheme_D'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Paymentsscheme_D');

-- Add a system role for privilege 'Niva_Paymentsscheme_D'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Paymentsscheme_D', 'Δικαίωμα διαγραφής στην οθόνη Manage Aid Schemes για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Paymentsscheme_D'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα διαγραφής στην οθόνη Manage Aid Schemes για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Paymentsscheme_D'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα διαγραφής στην οθόνη Manage Aid Schemes για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Paymentsscheme_D');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Paymentsscheme_D'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Paymentsscheme_D') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Paymentsscheme_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Paymentsscheme_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Paymentsscheme_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Paymentsscheme_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Paymentsscheme_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Paymentsscheme_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Paymentsscheme_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Paymentsscheme_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_Brerun_R

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Brerun_R'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Brerun_R');

-- Add a system role for privilege 'Niva_Brerun_R'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Brerun_R', 'Δικαίωμα ανάγνωσης στην οθόνη BRE Runs για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Brerun_R'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα ανάγνωσης στην οθόνη BRE Runs για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Brerun_R'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα ανάγνωσης στην οθόνη BRE Runs για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Brerun_R');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Brerun_R'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Brerun_R') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Brerun_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Brerun_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Brerun_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Brerun_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Brerun_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Brerun_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Brerun_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Brerun_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the guest system role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
    (syro_id, sypr_id, modu_id)
    SELECT (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
           (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Brerun_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
           (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
     WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
                        WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                          and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Brerun_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                       );

-- Add privilege Niva_Brerun_W

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Brerun_W'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Brerun_W');

-- Add a system role for privilege 'Niva_Brerun_W'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Brerun_W', 'Δικαίωμα εγγραφής στην οθόνη BRE Runs για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Brerun_W'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα εγγραφής στην οθόνη BRE Runs για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Brerun_W'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα εγγραφής στην οθόνη BRE Runs για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Brerun_W');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Brerun_W'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Brerun_W') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Brerun_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Brerun_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Brerun_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Brerun_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Brerun_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Brerun_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Brerun_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Brerun_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_Brerun_D

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Brerun_D'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Brerun_D');

-- Add a system role for privilege 'Niva_Brerun_D'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Brerun_D', 'Δικαίωμα διαγραφής στην οθόνη BRE Runs για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Brerun_D'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα διαγραφής στην οθόνη BRE Runs για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Brerun_D'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα διαγραφής στην οθόνη BRE Runs για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Brerun_D');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Brerun_D'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Brerun_D') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Brerun_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Brerun_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Brerun_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Brerun_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Brerun_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Brerun_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Brerun_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Brerun_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_AuxDecision_R

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_AuxDecision_R'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_AuxDecision_R');

-- Add a system role for privilege 'Niva_AuxDecision_R'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_AuxDecision_R', 'Δικαίωμα ανάγνωσης στην οθόνη Auxiliary Data Decisions για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_AuxDecision_R'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα ανάγνωσης στην οθόνη Auxiliary Data Decisions για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_AuxDecision_R'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα ανάγνωσης στην οθόνη Auxiliary Data Decisions για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_AuxDecision_R');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_AuxDecision_R'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_AuxDecision_R') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_AuxDecision_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AuxDecision_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_AuxDecision_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AuxDecision_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AuxDecision_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AuxDecision_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AuxDecision_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AuxDecision_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the guest system role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
    (syro_id, sypr_id, modu_id)
    SELECT (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
           (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AuxDecision_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
           (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
     WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
                        WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                          and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AuxDecision_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                       );

-- Add privilege Niva_AuxDecision_W

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_AuxDecision_W'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_AuxDecision_W');

-- Add a system role for privilege 'Niva_AuxDecision_W'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_AuxDecision_W', 'Δικαίωμα εγγραφής στην οθόνη Auxiliary Data Decisions για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_AuxDecision_W'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα εγγραφής στην οθόνη Auxiliary Data Decisions για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_AuxDecision_W'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα εγγραφής στην οθόνη Auxiliary Data Decisions για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_AuxDecision_W');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_AuxDecision_W'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_AuxDecision_W') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_AuxDecision_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AuxDecision_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_AuxDecision_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AuxDecision_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AuxDecision_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AuxDecision_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AuxDecision_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AuxDecision_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_AuxDecision_D

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_AuxDecision_D'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_AuxDecision_D');

-- Add a system role for privilege 'Niva_AuxDecision_D'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_AuxDecision_D', 'Δικαίωμα διαγραφής στην οθόνη Auxiliary Data Decisions για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_AuxDecision_D'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα διαγραφής στην οθόνη Auxiliary Data Decisions για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_AuxDecision_D'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα διαγραφής στην οθόνη Auxiliary Data Decisions για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_AuxDecision_D');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_AuxDecision_D'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_AuxDecision_D') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_AuxDecision_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AuxDecision_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_AuxDecision_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AuxDecision_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AuxDecision_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AuxDecision_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AuxDecision_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AuxDecision_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_AgrisnapUser_R

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_AgrisnapUser_R'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_AgrisnapUser_R');

-- Add a system role for privilege 'Niva_AgrisnapUser_R'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_AgrisnapUser_R', 'Δικαίωμα ανάγνωσης στην οθόνη Agrisnap Users για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_AgrisnapUser_R'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα ανάγνωσης στην οθόνη Agrisnap Users για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_AgrisnapUser_R'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα ανάγνωσης στην οθόνη Agrisnap Users για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_AgrisnapUser_R');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_AgrisnapUser_R'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_AgrisnapUser_R') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_AgrisnapUser_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUser_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_AgrisnapUser_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUser_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUser_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUser_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUser_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUser_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the guest system role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
    (syro_id, sypr_id, modu_id)
    SELECT (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
           (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUser_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
           (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
     WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
                        WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                          and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUser_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                       );

-- Add privilege Niva_AgrisnapUser_W

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_AgrisnapUser_W'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_AgrisnapUser_W');

-- Add a system role for privilege 'Niva_AgrisnapUser_W'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_AgrisnapUser_W', 'Δικαίωμα εγγραφής στην οθόνη Agrisnap Users για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_AgrisnapUser_W'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα εγγραφής στην οθόνη Agrisnap Users για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_AgrisnapUser_W'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα εγγραφής στην οθόνη Agrisnap Users για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_AgrisnapUser_W');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_AgrisnapUser_W'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_AgrisnapUser_W') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_AgrisnapUser_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUser_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_AgrisnapUser_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUser_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUser_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUser_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUser_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUser_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_AgrisnapUser_D

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_AgrisnapUser_D'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_AgrisnapUser_D');

-- Add a system role for privilege 'Niva_AgrisnapUser_D'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_AgrisnapUser_D', 'Δικαίωμα διαγραφής στην οθόνη Agrisnap Users για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_AgrisnapUser_D'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα διαγραφής στην οθόνη Agrisnap Users για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_AgrisnapUser_D'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα διαγραφής στην οθόνη Agrisnap Users για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_AgrisnapUser_D');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_AgrisnapUser_D'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_AgrisnapUser_D') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_AgrisnapUser_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUser_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_AgrisnapUser_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUser_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUser_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUser_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUser_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUser_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_Producer_R

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Producer_R'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Producer_R');

-- Add a system role for privilege 'Niva_Producer_R'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Producer_R', 'Δικαίωμα ανάγνωσης στην οθόνη Producer για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Producer_R'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα ανάγνωσης στην οθόνη Producer για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Producer_R'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα ανάγνωσης στην οθόνη Producer για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Producer_R');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Producer_R'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Producer_R') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Producer_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producer_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Producer_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producer_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producer_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producer_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producer_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producer_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the guest system role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
    (syro_id, sypr_id, modu_id)
    SELECT (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
           (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producer_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
           (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
     WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
                        WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                          and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producer_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                       );

-- Add privilege Niva_Producer_W

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Producer_W'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Producer_W');

-- Add a system role for privilege 'Niva_Producer_W'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Producer_W', 'Δικαίωμα εγγραφής στην οθόνη Producer για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Producer_W'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα εγγραφής στην οθόνη Producer για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Producer_W'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα εγγραφής στην οθόνη Producer για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Producer_W');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Producer_W'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Producer_W') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Producer_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producer_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Producer_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producer_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producer_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producer_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producer_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producer_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_Producer_D

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Producer_D'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Producer_D');

-- Add a system role for privilege 'Niva_Producer_D'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Producer_D', 'Δικαίωμα διαγραφής στην οθόνη Producer για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Producer_D'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα διαγραφής στην οθόνη Producer για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Producer_D'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα διαγραφής στην οθόνη Producer για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Producer_D');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Producer_D'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Producer_D') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Producer_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producer_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Producer_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producer_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producer_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producer_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producer_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producer_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_AfterLogin_R

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_AfterLogin_R'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_AfterLogin_R');

-- Add a system role for privilege 'Niva_AfterLogin_R'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_AfterLogin_R', 'Δικαίωμα ανάγνωσης στην οθόνη Welcome to NIVA DSS για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_AfterLogin_R'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα ανάγνωσης στην οθόνη Welcome to NIVA DSS για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_AfterLogin_R'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα ανάγνωσης στην οθόνη Welcome to NIVA DSS για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_AfterLogin_R');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_AfterLogin_R'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_AfterLogin_R') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_AfterLogin_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AfterLogin_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_AfterLogin_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AfterLogin_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AfterLogin_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AfterLogin_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AfterLogin_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AfterLogin_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the guest system role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
    (syro_id, sypr_id, modu_id)
    SELECT (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
           (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AfterLogin_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
           (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
     WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
                        WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                          and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AfterLogin_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                       );

-- Add privilege Niva_AfterLogin_W

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_AfterLogin_W'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_AfterLogin_W');

-- Add a system role for privilege 'Niva_AfterLogin_W'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_AfterLogin_W', 'Δικαίωμα εγγραφής στην οθόνη Welcome to NIVA DSS για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_AfterLogin_W'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα εγγραφής στην οθόνη Welcome to NIVA DSS για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_AfterLogin_W'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα εγγραφής στην οθόνη Welcome to NIVA DSS για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_AfterLogin_W');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_AfterLogin_W'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_AfterLogin_W') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_AfterLogin_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AfterLogin_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_AfterLogin_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AfterLogin_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AfterLogin_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AfterLogin_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AfterLogin_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AfterLogin_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_AfterLogin_D

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_AfterLogin_D'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_AfterLogin_D');

-- Add a system role for privilege 'Niva_AfterLogin_D'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_AfterLogin_D', 'Δικαίωμα διαγραφής στην οθόνη Welcome to NIVA DSS για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_AfterLogin_D'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα διαγραφής στην οθόνη Welcome to NIVA DSS για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_AfterLogin_D'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα διαγραφής στην οθόνη Welcome to NIVA DSS για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_AfterLogin_D');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_AfterLogin_D'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_AfterLogin_D') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_AfterLogin_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AfterLogin_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_AfterLogin_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AfterLogin_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AfterLogin_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AfterLogin_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AfterLogin_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AfterLogin_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_Tools_R

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Tools_R'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Tools_R');

-- Add a system role for privilege 'Niva_Tools_R'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Tools_R', 'Δικαίωμα ανάγνωσης στην οθόνη Tools για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Tools_R'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα ανάγνωσης στην οθόνη Tools για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Tools_R'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα ανάγνωσης στην οθόνη Tools για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Tools_R');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Tools_R'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Tools_R') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Tools_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Tools_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Tools_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Tools_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Tools_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Tools_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Tools_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Tools_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the guest system role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
    (syro_id, sypr_id, modu_id)
    SELECT (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
           (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Tools_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
           (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
     WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
                        WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                          and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Tools_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                       );

-- Add privilege Niva_Tools_W

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Tools_W'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Tools_W');

-- Add a system role for privilege 'Niva_Tools_W'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Tools_W', 'Δικαίωμα εγγραφής στην οθόνη Tools για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Tools_W'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα εγγραφής στην οθόνη Tools για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Tools_W'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα εγγραφής στην οθόνη Tools για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Tools_W');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Tools_W'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Tools_W') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Tools_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Tools_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Tools_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Tools_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Tools_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Tools_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Tools_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Tools_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_Tools_D

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Tools_D'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Tools_D');

-- Add a system role for privilege 'Niva_Tools_D'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Tools_D', 'Δικαίωμα διαγραφής στην οθόνη Tools για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Tools_D'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα διαγραφής στην οθόνη Tools για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Tools_D'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα διαγραφής στην οθόνη Tools για την εφαρμογή NIVA - Use Case 1a: Decision Support System V3';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Tools_D');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Tools_D'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Tools_D') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Tools_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Tools_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Tools_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Tools_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Tools_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Tools_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Tools_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Tools_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);



-- add roles for multipage system roles to every subscriber has access to module 'Niva'
select subscription.add_multipage_roles(a.subs_id, a.modu_name)
  from 
(select distinct subs.subs_id, modu.modu_name
  from subscription.ss_subscribers subs
  	join subscription.ss_subscribers_module_groups sumg on sumg.subs_id = subs.subs_id
    join subscription.ss_grouping_modules grmo on grmo.mogr_id = sumg.mogr_id
    join subscription.ss_modules modu on modu.modu_id = grmo.modu_id
 where modu.modu_name = 'Niva') a;
 

-- Insert wsPaths for logging 
insert into ws_responses.ss_module_wss(modu_id, mows_ws_path)
with paths AS (
select unnest(
ARRAY[
'Login/loginUser',
'Menu/getMenu',
'Menu/getBannerMessages',
'Menu/getLoginPageMessages',
'Menu/getAuditInfoUsers',
'MainService/synchronizeChangesWithDb_ParcelsImport',
'MainService/synchronizeChangesWithDb_Classifier',
'MainService/synchronizeChangesWithDb_CoverType',
'MainService/synchronizeChangesWithDb_Cultivation',
'MainService/synchronizeChangesWithDb_Document',
'MainService/synchronizeChangesWithDb_FileTemplate',
'MainService/synchronizeChangesWithDb_SystemConfiguration',
'MainService/synchronizeChangesWithDb_UserNotification',
'MainService/synchronizeChangesWithDb_EcSet',
'MainService/synchronizeChangesWithDb_Paymentsscheme',
'MainService/synchronizeChangesWithDb_Brerun',
'MainService/synchronizeChangesWithDb_AuxDecision',
'MainService/synchronizeChangesWithDb_AgrisnapUser',
'MainService/synchronizeChangesWithDb_Producer',
'MainService/checkProducerStatus',
'MainService/getProducerStatus',
'MainService/getLogoProvider',
'MainService/setActiveSystemConfigurationProfile',
'MainService/afterloginActiveSystemConfigurationProfile',
'MainService/readParcelsData',
'MainService/checknconnectParcelsData',
'MainService/importParcelsData',
'MainService/runBRE',
'MainService/updatePaymentsSchemeWithBRERunResults',
'MainService/lockSelectedParcelsPaymentsScheme',
'MainService/getAgrisnapRequests',
'MainService/uploadGeotaggedPhoto',
'MainService/getFmisRequests',
'MainService/uploadFmisDocument',
'MainService/updateStatusWithAuxDecision',
'MainService/getBREresultsToCSV',
'MainService/importExcel_CoverType_impCoTys_id',
'MainService/importExcel_CoverType_impCoTys_id_SpecsAndSample',
'MainService/importExcel_Cultivation_impCults_id',
'MainService/importExcel_Cultivation_impCults_id_SpecsAndSample',
'AgrisnapUser/findAllByIds',
'AgrisnapUser/findByAgrisnapuserId',
'AgrisnapUser/findLazyAgrisnapUser',
'AgrisnapUser/delByAgrisnapuserId',
'AuxDecision/findAllByIds',
'AuxDecision/findByAuxdecisionId',
'AuxDecision/findLazyAuxDecision',
'AuxDecision/delByAuxdecisionId',
'Brerun/findAllByIds',
'Brerun/findByBrerunId',
'Brerun/findLazyBrerun',
'Brerun/delByBrerunId',
'Classifier/findAllByIds',
'Classifier/findByClassifierId',
'Classifier/findLazyClassifier',
'Classifier/findAllByCriteriaRange_forLov',
'Classifier/delByClassifierId',
'EcSet/findAllByIds',
'EcSet/findByEcsetId',
'EcSet/findLazyEcSet',
'EcSet/findAllByCriteriaRange_forLov',
'EcSet/delByEcsetId',
'FileTemplate/findAllByIds',
'FileTemplate/findByFiletemplateId',
'FileTemplate/findLazyFileTemplate',
'FileTemplate/findAllByCriteriaRange_forLov',
'FileTemplate/delByFiletemplateId',
'ParcelsImport/findAllByIds',
'ParcelsImport/findByParcelsimportId',
'ParcelsImport/findLazyParcelsImport',
'ParcelsImport/getAttachedFile',
'ParcelsImport/setAttachedFile',
'ParcelsImport/findAllByCriteriaRange_Brerun_GrpBrerun_itm__parcelsimportId',
'ParcelsImport/findAllByCriteriaRange_forLov',
'ParcelsImport/delByParcelsimportId',
'Paymentsscheme/findAllByIds',
'Paymentsscheme/findByPaymentsschemeId',
'Paymentsscheme/findLazyPaymentsscheme',
'Paymentsscheme/findAllByCriteriaRange_Brerun_GrpBrerun_itm__6',
'Paymentsscheme/delByPaymentsschemeId',
'Producer/findAllByIds',
'Producer/findByProducerId',
'Producer/findLazyProducer',
'Producer/findAllByCriteriaRange_forLov',
'Producer/delByProducerId',
'UserNotification/findAllByIds',
'UserNotification/findByUserNotificationId',
'UserNotification/findLazyUserNotification',
'UserNotification/delByUserNotificationId'
]) as ws_path) 
select modu.modu_id, paths.ws_path
  from subscription.ss_modules modu
  	cross join paths
 where modu.modu_name = 'Niva'
   and not exists (select 1
                     from ws_responses.ss_module_wss m
                    where m.modu_id = modu.modu_id
                      and m.mows_ws_path = paths.ws_path
                   );


end transaction;
