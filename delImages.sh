#
# Copyright (c) OPEKEPE 2021 -- 2021. All rights reserved
#
# Project and code is made available under the EU-PL license.
#
./stop.sh
docker rm -v uc1a_docker_backend_1
docker rm -v uc1a_docker_db_1
docker rm -v uc1a_docker_memcached_1

