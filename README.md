# Introduction

This sub-project is part of the ["New IACS Vision in Action” --- NIVA](https://www.niva4cap.eu/) project that delivers a  a suite of digital solutions, e-tools and good practices for e-governance and 
initiates an innovation ecosystem to support further development of IACS that will facilitate data and information flows.

This project has received funding from the European Union’s Horizon 2020 research and innovation programme under 
grant agreement No 842009.

Please visit the [website](https://www.niva4cap.eu) for further information. A complete list of the sub-projects made
available under the NIVA project can be found in [gitlab](https://gitlab.com/nivaeu/)


# niva-docker

This project provides the appropriate dockerised-condainers for running the Niva DSS (UC1a)

# Prerequisites

You should install docker and docker-compose in order to run this project.

Volume handling on docker is easier on Un*x machines. If you want to run this on a Windows machine it is better to use docker with a WSL-2 based engine.

If you already running a postgres image on port 5432 or any other images on ports `8080`, `11211`, `8009`, `8443`, `9990` check the Parameterisation section

# How to run

	git clone https://gitlab.com/nivaeu/uc1a_docker.git
        ./startup.sh

UC1a will be available at http://localhost:8080/Niva

Some predefined users are created such as u1@Niva, u2@Niva and so on.

All users have the same password "user1"

# Cleanup

	./clean.sh

will clean everything (including stoping, removing the containers and deleting your uc1a data)

	./stop.sh

will stop the containers


# Parameterisation

If you want to publish it on a different port (e.g. 80), should should edit line 28 of docker-compose.yml from something like
`- "8080:8080" ` to `- "80:8080"`

Similarly if you want to change any other port


# Source code

In case you want to build and setup it from scratch, the source code and installation instructions are available at <https://gitlab.com/nivaeu/uc1a>

# Installation Problems

If you have installation problems, check first https://gitlab.com/nivaeu/uc1a_docker/-/wikis/Installation-Problems
