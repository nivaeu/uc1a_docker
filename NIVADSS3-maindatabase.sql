-- NIVA DSS 3 Main Database
-- NEUROPUBLIC / MT
-- v20221026a FULL

/**
*
* © 2022 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/


-- THIS FILE USES niva3b AS DATABASE's NAME.
--	Replace this with whatever is preferred. 

--
-- Use the next lines to delete the Schema or the Database if already exists
--
-- W A R N I N G !!
-- A L L  D A T A  W I L L  B E  L O S T
--
--

--DROP DATABASE niva3b;
--DROP SCHEMA IF EXISTS niva CASCADE;


--CREATE DATABASE niva3b WITH TEMPLATE = template0 ENCODING = 'UTF8';

ALTER DATABASE niva3b OWNER TO postgres;

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE niva3b; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE niva3b IS 'NIVA v3 Database with Major Changes.


Neuropublic SA / Michail Tziotis';

--
-- Name: niva; Type: SCHEMA; Schema: -; Owner: postgres
--


CREATE SCHEMA niva;
ALTER SCHEMA niva OWNER TO postgres;


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;

-- CREATE EXTENSION postgis SET SCHEMA niva;

--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


--
-- Name: import_crops(character varying, integer, character varying, integer); Type: FUNCTION; Schema: niva; Owner: postgres
--

CREATE FUNCTION niva.import_crops(i_name character varying, i_code integer, i_covertype_name character varying, i_excelfile_id integer) RETURNS numeric
    LANGUAGE plpgsql
    AS $$

-- MT 20220214a
DECLARE
    n_name VARCHAR(200);
    n_code INTEGER;
	n_covertype_id INTEGER;

BEGIN
	
	
	-- Check if a Crop with the same name already exists with another code
	-- or if a Crop with the same code already exists with another name.
	
	select name, code
     into n_name, n_code
     from niva.cultivations
    where (name = i_name and code !=i_code) or (name!=i_name and code = i_code);
     if found then
		case n_name=i_name
			when true then
				RAISE EXCEPTION 'Crop Name "%" With Code "%" Already Exists With Code "%"', i_name, i_code, n_code;
			else
				RAISE EXCEPTION 'Crop Code "%" With Name "%" Already Exists With Name "%"', i_code, i_name, n_name;
		end case;
     end if;
	
	
	-- Check if a Crop with the same code & name already exists (Do nothing).
	select name, code
     into n_name, n_code
     from niva.cultivations
    where (name = i_name and code =i_code);
     if not found then
  
		-- Check if a Land Cover with the name declared already exists.
		select cp.covertype_id
		into n_covertype_id
		from niva.cover_types cp
		where cp.name = i_covertype_name;
		if not found then
			RAISE EXCEPTION 'Land Cover Named "%" Does Not Exist', i_covertype_name;
		end if;
			
		
		INSERT INTO  niva.cultivations(
				covertype_id, 
				name,
				code,
				excelfile_id
			)
			VALUES (
				n_covertype_id,
				i_name,
				i_code,
				i_excelfile_id                              
			);

  
     end if;

  return 0;

END;

$$;


ALTER FUNCTION niva.import_crops(i_name character varying, i_code integer, i_covertype_name character varying, i_excelfile_id integer) OWNER TO postgres;

--
-- Name: FUNCTION import_crops(i_name character varying, i_code integer, i_covertype_name character varying, i_excelfile_id integer); Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON FUNCTION niva.import_crops(i_name character varying, i_code integer, i_covertype_name character varying, i_excelfile_id integer) IS 'Imports new crops into the list.';


--
-- Name: import_land_covers(integer, character varying, integer); Type: FUNCTION; Schema: niva; Owner: postgres
--

CREATE FUNCTION niva.import_land_covers(i_code integer, i_name character varying, i_excel_id integer) RETURNS numeric
    LANGUAGE plpgsql
    AS $$-- NEUROPUBLIC SA / Michail Tziotis
-- MT 20220211a
DECLARE
    n_code INTEGER;   
    n_name VARCHAR(60); 
BEGIN
	
	
	-- Check if a Land Cover with the same name already exists with another code
	-- or if a Land Cover with the same code already exists with another name.
	
	select name, code
     into n_name, n_code
     from niva.cover_types
    where (name = i_name and code !=i_code) or (name!=i_name and code = i_code);
     if found then
		case n_name=i_name
			when true then
				RAISE EXCEPTION 'Land Cover Name "%" With Code "%" Already Exists With Code "%"', i_name, i_code, n_code;
			else
				RAISE EXCEPTION 'Land Cover Code "%" With Name "%" Already Exists With Name "%"', i_code, i_name, n_name;
		end case;
     end if;
	
	
	
	
	-- Check if a Land Cover with the same code & name already exists (Do nothing).
	select name, code
     into n_name, n_code
     from niva.cover_types
    where (name = i_name and code =i_code);
     if not found then
      
          INSERT INTO  niva.cover_types( code,
                                  name,
                                  excelfile_id
                                )
                            VALUES (i_code,
                                    i_name,
                                    i_excel_id                              
                            );
		end if;
  return 0;

END;
$$;


ALTER FUNCTION niva.import_land_covers(i_code integer, i_name character varying, i_excel_id integer) OWNER TO postgres;

--
-- Name: selectActiveSystemConfigurationProfile(integer); Type: FUNCTION; Schema: niva; Owner: postgres
--

CREATE FUNCTION niva."selectActiveSystemConfigurationProfile"(systemconfigurationid integer) RETURNS TABLE(systemconfigurationidactive integer, name character varying, description character varying, agency character varying, language character varying, srid smallint, tmpdir character varying, documentationmaxfilesize integer)
    LANGUAGE plpgsql
    AS $$

-- FUNCTION: niva.selectActiveSystemConfigurationProfile(integer)
-- NEUROPUBLIC SA / Michail Tziotis
-- v20220208a

    BEGIN
        IF systemconfigurationid <> 0 THEN
		UPDATE niva."system_configurations"
        	SET active=false;
        UPDATE niva.system_configurations
          	SET active=true
          	WHERE "system_configurations"."systemconfiguration_id"=systemconfigurationId;	
        END IF;
		RETURN QUERY SELECT sc.systemconfiguration_id as systemconfigurationidactive, sc.name, sc.description,sc.agency,sc.language, sc. srid, sc.tmpdir, sc.documentationmaxfilesize FROM niva.system_configurations sc WHERE active=true;	
    END;
$$;


ALTER FUNCTION niva."selectActiveSystemConfigurationProfile"(systemconfigurationid integer) OWNER TO postgres;

--
-- Name: niva_sq; Type: SEQUENCE; Schema: niva; Owner: postgres
--

CREATE SEQUENCE niva.niva_sq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999999999
    CACHE 1;


ALTER TABLE niva.niva_sq OWNER TO postgres;

--
-- Name: SEQUENCE niva_sq; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON SEQUENCE niva.niva_sq IS 'The main sequence of NIVA DSS.';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: agrisnap_requests; Type: TABLE; Schema: niva; Owner: postgres
--

CREATE TABLE niva.agrisnap_requests (
    agrisnaprequest_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    datetime timestamp with time zone DEFAULT now() NOT NULL,
    agrisnap_uid character(5) NOT NULL,
    usrinsert character varying(150),
    dteinsert timestamp with time zone DEFAULT now(),
    usrupdate character varying(150),
    dteupdate timestamp with time zone DEFAULT now(),
    row_version integer DEFAULT 0 NOT NULL
);


ALTER TABLE niva.agrisnap_requests OWNER TO postgres;

--
-- Name: TABLE agrisnap_requests; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON TABLE niva.agrisnap_requests IS 'Contains the requests by the client and the corresponding responses.';


--
-- Name: agrisnap_requests_contexts; Type: TABLE; Schema: niva; Owner: postgres
--

CREATE TABLE niva.agrisnap_requests_contexts (
    agrisnap_requestscontext_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    agrisnap_request_id integer NOT NULL,
    comment character varying,
    parcelsstatus_id integer NOT NULL,
    usrinsert character varying(150),
    dteinsert timestamp with time zone DEFAULT now(),
    usrupdate character varying(150),
    dteupdate timestamp with time zone DEFAULT now(),
    row_version integer DEFAULT 0 NOT NULL
);


ALTER TABLE niva.agrisnap_requests_contexts OWNER TO postgres;

--
-- Name: TABLE agrisnap_requests_contexts; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON TABLE niva.agrisnap_requests_contexts IS 'Contains the contexts related to Agrisnap requests.';


--
-- Name: agrisnap_users; Type: TABLE; Schema: niva; Owner: postgres
--

CREATE TABLE niva.agrisnap_users (
    agrisnap_name character varying(40) NOT NULL,
    row_version integer DEFAULT 0 NOT NULL,
    agrisnapuser_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    agrisnap_uid character(5) NOT NULL,
    usrinsert character varying(150),
    dteinsert timestamp with time zone DEFAULT now(),
    usrupdate character varying(150),
    dteupdate timestamp with time zone DEFAULT now()
);


ALTER TABLE niva.agrisnap_users OWNER TO postgres;

--
-- Name: TABLE agrisnap_users; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON TABLE niva.agrisnap_users IS 'Contains the Users of Agrisnap. Each User can be associated with more than one Producers.';


--
-- Name: agrisnap_usersproducers; Type: TABLE; Schema: niva; Owner: postgres
--

CREATE TABLE niva.agrisnap_usersproducers (
    agrisnapusersproducer_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    agrisnapuser_id integer NOT NULL,
    producer_id integer NOT NULL,
    usrinsert character varying(150),
    dteinsert timestamp with time zone DEFAULT now(),
    usrupdate character varying(150),
    dteupdate timestamp with time zone DEFAULT now(),
    row_version integer DEFAULT 0 NOT NULL
);


ALTER TABLE niva.agrisnap_usersproducers OWNER TO postgres;

--
-- Name: TABLE agrisnap_usersproducers; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON TABLE niva.agrisnap_usersproducers IS 'Contains the associations of Agrisnap Users with Producers.';


--
-- Name: aux_decisions; Type: TABLE; Schema: niva; Owner: postgres
--

CREATE TABLE niva.aux_decisions (
    usrinsert character varying(150),
    dteinsert timestamp with time zone DEFAULT now(),
    usrupdate character varying(150),
    dteupdate timestamp with time zone DEFAULT now(),
    row_version integer DEFAULT 0 NOT NULL,
    level1decision integer,
    level2decision integer,
    auxdecision_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    uniqueparcel_id integer NOT NULL,
    level1decisioncheck boolean GENERATED ALWAYS AS ((level1decision IS NOT NULL)) STORED NOT NULL,
    level2decisioncheck boolean GENERATED ALWAYS AS ((level2decision IS NOT NULL)) STORED NOT NULL,
    newgp boolean NOT NULL,
    newfmis boolean NOT NULL
);


ALTER TABLE niva.aux_decisions OWNER TO postgres;

--
-- Name: TABLE aux_decisions; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON TABLE niva.aux_decisions IS 'Contains the decisions per parcel,  year, and evaluation level.';


--
-- Name: breruns; Type: TABLE; Schema: niva; Owner: postgres
--

CREATE TABLE niva.breruns (
    brerun_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    parcelsimport_id integer NOT NULL,
    ecset_id integer NOT NULL,
    description character varying(60),
    date_time timestamp with time zone DEFAULT now(),
    usrinsert character varying(150),
    dteinsert timestamp with time zone DEFAULT now(),
    usrupdate character varying(150),
    dteupdate timestamp with time zone DEFAULT now(),
    row_version integer DEFAULT 0 NOT NULL,
    has_been_run boolean DEFAULT false
);


ALTER TABLE niva.breruns OWNER TO postgres;

--
-- Name: TABLE breruns; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON TABLE niva.breruns IS 'Contains the BRE Runs.';


--
-- Name: COLUMN breruns.brerun_id; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.breruns.brerun_id IS 'primary key';


--
-- Name: COLUMN breruns.parcelsimport_id; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.breruns.parcelsimport_id IS 'fk to classification';


--
-- Name: COLUMN breruns.ecset_id; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.breruns.ecset_id IS 'fk to ec_group';


--
-- Name: COLUMN breruns.has_been_run; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.breruns.has_been_run IS 'Flag True if Decision Making has been run.';


--
-- Name: classifiers; Type: TABLE; Schema: niva; Owner: postgres
--

CREATE TABLE niva.classifiers (
    classifier_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    name character varying(60) NOT NULL,
    description character varying(60),
    usrinsert character varying(150),
    dteinsert timestamp with time zone DEFAULT now(),
    usrupdate character varying(150),
    dteupdate timestamp with time zone DEFAULT now(),
    row_version integer DEFAULT 0 NOT NULL
);


ALTER TABLE niva.classifiers OWNER TO postgres;

--
-- Name: TABLE classifiers; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON TABLE niva.classifiers IS 'Contains the Classifiers, like Sen4CAP.';


--
-- Name: COLUMN classifiers.classifier_id; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.classifiers.classifier_id IS 'primary key';


--
-- Name: COLUMN classifiers.name; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.classifiers.name IS 'classifier name';


--
-- Name: COLUMN classifiers.description; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.classifiers.description IS 'classifier description';


--
-- Name: cover_types; Type: TABLE; Schema: niva; Owner: postgres
--

CREATE TABLE niva.cover_types (
    covertype_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    code integer NOT NULL,
    name character varying(60) NOT NULL,
    usrinsert character varying(150),
    dteinsert timestamp with time zone DEFAULT now(),
    usrupdate character varying(150),
    dteupdate timestamp with time zone DEFAULT now(),
    row_version integer DEFAULT 0 NOT NULL,
    excelfile_id integer
);


ALTER TABLE niva.cover_types OWNER TO postgres;

--
-- Name: TABLE cover_types; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON TABLE niva.cover_types IS 'Contains the Cover Types.';


--
-- Name: COLUMN cover_types.covertype_id; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.cover_types.covertype_id IS 'primary key';


--
-- Name: COLUMN cover_types.code; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.cover_types.code IS 'code of crop';


--
-- Name: COLUMN cover_types.name; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.cover_types.name IS 'crop name';


--
-- Name: cultivations; Type: TABLE; Schema: niva; Owner: postgres
--

CREATE TABLE niva.cultivations (
    cultivation_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    covertype_id integer NOT NULL,
    name character varying(200) NOT NULL,
    code integer NOT NULL,
    usrinsert character varying(150),
    dteinsert timestamp with time zone DEFAULT now(),
    usrupdate character varying(150),
    dteupdate timestamp with time zone DEFAULT now(),
    row_version integer DEFAULT 0 NOT NULL,
    excelfile_id integer
);


ALTER TABLE niva.cultivations OWNER TO postgres;

--
-- Name: TABLE cultivations; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON TABLE niva.cultivations IS 'Contains the Cultivations.';


--
-- Name: COLUMN cultivations.cultivation_id; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.cultivations.cultivation_id IS 'primary key';


--
-- Name: COLUMN cultivations.covertype_id; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.cultivations.covertype_id IS 'fk to cover_type';


--
-- Name: COLUMN cultivations.name; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.cultivations.name IS 'cultivation name';


--
-- Name: COLUMN cultivations.code; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.cultivations.code IS 'code of cultivation';


--
-- Name: COLUMN cultivations.excelfile_id; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.cultivations.excelfile_id IS 'fk to excel_files';


--
-- Name: dbmanagementlogs; Type: TABLE; Schema: niva; Owner: postgres
--

CREATE TABLE niva.dbmanagementlogs (
    dbml_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    dte_done timestamp(0) with time zone DEFAULT now() NOT NULL,
    version character varying(10) NOT NULL,
    notes character varying,
    type integer,
    usrinsert character varying(150),
    dteinsert timestamp with time zone DEFAULT now(),
    usrupdate character varying(150),
    dteupdate timestamp with time zone DEFAULT now(),
    row_version integer DEFAULT 0 NOT NULL
);


ALTER TABLE niva.dbmanagementlogs OWNER TO postgres;

--
-- Name: TABLE dbmanagementlogs; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON TABLE niva.dbmanagementlogs IS 'LOOKUP Do not generate form.
Contains info about any updates of the db schema etc.';


--
-- Name: COLUMN dbmanagementlogs.type; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.dbmanagementlogs.type IS 'Contains the log type. 
1= Started.
2= Succeeded.';


--
-- Name: documents; Type: TABLE; Schema: niva; Owner: postgres
--

CREATE TABLE niva.documents (
    document_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    attached_file bytea,
    file_name character varying(250) NOT NULL,
    description character varying(60),
    usrinsert character varying(150),
    dteinsert timestamp with time zone DEFAULT now(),
    usrupdate character varying(150),
    dteupdate timestamp with time zone DEFAULT now(),
    row_version integer DEFAULT 0 NOT NULL
);


ALTER TABLE niva.documents OWNER TO postgres;

--
-- Name: TABLE documents; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON TABLE niva.documents IS 'Contains the Help Documents.';


--
-- Name: ec_sets; Type: TABLE; Schema: niva; Owner: postgres
--

CREATE TABLE niva.ec_sets (
    ecset_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    description character varying(60),
    name character varying(60) NOT NULL,
    usrinsert character varying(150),
    dteinsert timestamp without time zone DEFAULT now(),
    usrupdate character varying(150),
    dteupdate timestamp without time zone DEFAULT now(),
    row_version integer DEFAULT 0 NOT NULL,
    evaluationmethod smallint NOT NULL
);


ALTER TABLE niva.ec_sets OWNER TO postgres;

--
-- Name: TABLE ec_sets; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON TABLE niva.ec_sets IS 'Eligibility Criteria sets.';


--
-- Name: COLUMN ec_sets.ecset_id; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.ec_sets.ecset_id IS 'primary key';


--
-- Name: COLUMN ec_sets.name; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.ec_sets.name IS 'ec_group name';


--
-- Name: COLUMN ec_sets.evaluationmethod; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.ec_sets.evaluationmethod IS '1: Cultivation, 2: Crop Cover (General type), 3: Hybrid';


--
-- Name: ec_sets_cslcs; Type: TABLE; Schema: niva; Owner: postgres
--

CREATE TABLE niva.ec_sets_cslcs (
    ecsetsclc_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    cultivation_id integer,
    ecset_id integer,
    usrinsert character varying(150),
    dteinsert timestamp with time zone DEFAULT now(),
    usrupdate character varying(150),
    dteupdate timestamp with time zone DEFAULT now(),
    row_version integer DEFAULT 0 NOT NULL,
    covertype_id integer
);


ALTER TABLE niva.ec_sets_cslcs OWNER TO postgres;

--
-- Name: TABLE ec_sets_cslcs; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON TABLE niva.ec_sets_cslcs IS 'Eligibility Criteria sets for cultivations & land cover types.';


--
-- Name: COLUMN ec_sets_cslcs.ecsetsclc_id; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.ec_sets_cslcs.ecsetsclc_id IS 'primary key';


--
-- Name: COLUMN ec_sets_cslcs.cultivation_id; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.ec_sets_cslcs.cultivation_id IS 'fk to cultivation';


--
-- Name: COLUMN ec_sets_cslcs.ecset_id; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.ec_sets_cslcs.ecset_id IS 'fk to Ec_Group';


--
-- Name: COLUMN ec_sets_cslcs.covertype_id; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.ec_sets_cslcs.covertype_id IS 'fk to cover_type';


--
-- Name: ec_sets_cslcs_details; Type: TABLE; Schema: niva; Owner: postgres
--

CREATE TABLE niva.ec_sets_cslcs_details (
    ecsetscslcsdetail_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    ecsetsclc_id integer NOT NULL,
    ordering_number smallint,
    agrees_declar smallint,
    comparison_oper smallint,
    probab_thres numeric(4,2),
    decision_light smallint NOT NULL,
    usrinsert character varying(150),
    dteinsert timestamp(0) with time zone DEFAULT now(),
    usrupdate character varying(150),
    dteupdate timestamp(0) with time zone DEFAULT now(),
    row_version integer DEFAULT 0 NOT NULL,
    agrees_declar2 smallint,
    comparison_oper2 smallint,
    probab_thres2 numeric(4,2),
    probab_thres_sum numeric(4,2),
    comparison_oper3 smallint,
    CONSTRAINT ecsetscslcsdetail_ordering_number_greater_than CHECK ((ordering_number > 0))
);


ALTER TABLE niva.ec_sets_cslcs_details OWNER TO postgres;

--
-- Name: TABLE ec_sets_cslcs_details; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON TABLE niva.ec_sets_cslcs_details IS 'Eligibility Criteria of cultivations & land covers in detail.';


--
-- Name: COLUMN ec_sets_cslcs_details.ecsetscslcsdetail_id; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.ec_sets_cslcs_details.ecsetscslcsdetail_id IS 'primary key';


--
-- Name: COLUMN ec_sets_cslcs_details.ecsetsclc_id; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.ec_sets_cslcs_details.ecsetsclc_id IS 'fk to cultivation';


--
-- Name: COLUMN ec_sets_cslcs_details.ordering_number; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.ec_sets_cslcs_details.ordering_number IS 'AA (No.) increasing ordering number';


--
-- Name: COLUMN ec_sets_cslcs_details.agrees_declar; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.ec_sets_cslcs_details.agrees_declar IS '1: agree, 0: disagree declaration';


--
-- Name: COLUMN ec_sets_cslcs_details.comparison_oper; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.ec_sets_cslcs_details.comparison_oper IS '1: greater, 2: greater equal, 3:less, 4: less equal';


--
-- Name: COLUMN ec_sets_cslcs_details.probab_thres; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.ec_sets_cslcs_details.probab_thres IS 'probability threshold ';


--
-- Name: COLUMN ec_sets_cslcs_details.decision_light; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.ec_sets_cslcs_details.decision_light IS '1: green, 2: yellow, 3:red';


--
-- Name: COLUMN ec_sets_cslcs_details.agrees_declar2; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.ec_sets_cslcs_details.agrees_declar2 IS '1: agree, 0: disagree declaration';


--
-- Name: COLUMN ec_sets_cslcs_details.comparison_oper2; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.ec_sets_cslcs_details.comparison_oper2 IS '1: greater, 2: greater equal, 3:less, 4: less equal';


--
-- Name: COLUMN ec_sets_cslcs_details.probab_thres2; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.ec_sets_cslcs_details.probab_thres2 IS 'probability 2 threshold ';


--
-- Name: COLUMN ec_sets_cslcs_details.probab_thres_sum; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.ec_sets_cslcs_details.probab_thres_sum IS 'sum of probability 1+2 threshold ';


--
-- Name: COLUMN ec_sets_cslcs_details.comparison_oper3; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.ec_sets_cslcs_details.comparison_oper3 IS '1: greater, 2: greater equal, 3:less, 4: less equal';


--
-- Name: niva_sq_integer; Type: SEQUENCE; Schema: niva; Owner: postgres
--

CREATE SEQUENCE niva.niva_sq_integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999999999
    CACHE 1;


ALTER TABLE niva.niva_sq_integer OWNER TO postgres;

--
-- Name: SEQUENCE niva_sq_integer; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON SEQUENCE niva.niva_sq_integer IS 'Sequence for integers.';


--
-- Name: excel_errors; Type: TABLE; Schema: niva; Owner: postgres
--

CREATE TABLE niva.excel_errors (
    id integer DEFAULT nextval('niva.niva_sq_integer'::regclass) NOT NULL,
    exfi_id integer NOT NULL,
    excel_row_num integer,
    err_message character varying(500),
    usrinsert character varying(150),
    dteinsert timestamp with time zone DEFAULT now(),
    usrupdate character varying(150),
    dteupdate timestamp with time zone DEFAULT now(),
    row_version integer DEFAULT 0 NOT NULL
);


ALTER TABLE niva.excel_errors OWNER TO postgres;

--
-- Name: TABLE excel_errors; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON TABLE niva.excel_errors IS 'LOOKUP Do not generate form.
Excel errors.';


--
-- Name: COLUMN excel_errors.id; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.excel_errors.id IS 'primary key';


--
-- Name: COLUMN excel_errors.exfi_id; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.excel_errors.exfi_id IS 'fk to excel_files';


--
-- Name: COLUMN excel_errors.excel_row_num; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.excel_errors.excel_row_num IS 'excel_row_num';


--
-- Name: COLUMN excel_errors.err_message; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.excel_errors.err_message IS 'err_message';


--
-- Name: excel_files; Type: TABLE; Schema: niva; Owner: postgres
--

CREATE TABLE niva.excel_files (
    id integer DEFAULT nextval('niva.niva_sq_integer'::regclass) NOT NULL,
    filename character varying(120) NOT NULL,
    data bytea,
    total_rows integer,
    total_error_rows integer,
    usrinsert character varying(150),
    dteinsert timestamp with time zone DEFAULT now(),
    usrupdate character varying(150),
    dteupdate timestamp with time zone DEFAULT now(),
    row_version integer DEFAULT 0 NOT NULL
);


ALTER TABLE niva.excel_files OWNER TO postgres;

--
-- Name: TABLE excel_files; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON TABLE niva.excel_files IS 'LOOKUP Do not generate form.
Excel files.';


--
-- Name: COLUMN excel_files.id; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.excel_files.id IS 'primary key';


--
-- Name: COLUMN excel_files.filename; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.excel_files.filename IS 'name of file';


--
-- Name: COLUMN excel_files.data; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.excel_files.data IS 'upload file';


--
-- Name: COLUMN excel_files.total_rows; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.excel_files.total_rows IS 'total rows';


--
-- Name: COLUMN excel_files.total_error_rows; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.excel_files.total_error_rows IS 'total rows';


--
-- Name: file_templates; Type: TABLE; Schema: niva; Owner: postgres
--

CREATE TABLE niva.file_templates (
    filetemplate_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    name character varying(60) NOT NULL,
    usrinsert character varying(150),
    dteinsert timestamp with time zone DEFAULT now(),
    usrupdate character varying(150),
    dteupdate timestamp with time zone DEFAULT now(),
    row_version integer DEFAULT 0 NOT NULL,
    scn_identifier character varying NOT NULL,
    scn_parcelcode character varying NOT NULL,
    scn_producercode character varying NOT NULL,
    scn_cultivationdecl character varying NOT NULL,
    scn_cultivationidpred1 character varying NOT NULL,
    scn_probabilitypred1 character varying NOT NULL,
    scn_cultivationidpred2 character varying NOT NULL,
    scn_probabilitypred2 character varying NOT NULL
);


ALTER TABLE niva.file_templates OWNER TO postgres;

--
-- Name: TABLE file_templates; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON TABLE niva.file_templates IS 'Contains the templates of files to be imported.';


--
-- Name: COLUMN file_templates.filetemplate_id; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.file_templates.filetemplate_id IS 'primary key';


--
-- Name: COLUMN file_templates.name; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.file_templates.name IS 'template general name';


--
-- Name: fmis_requests; Type: TABLE; Schema: niva; Owner: postgres
--

CREATE TABLE niva.fmis_requests (
    fmisrequest_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    datetime timestamp with time zone DEFAULT now() NOT NULL,
    producer_id integer NOT NULL,
    usrinsert character varying(150),
    dteinsert timestamp with time zone DEFAULT now(),
    usrupdate character varying(150),
    dteupdate timestamp with time zone DEFAULT now(),
    row_version integer DEFAULT 0 NOT NULL
);


ALTER TABLE niva.fmis_requests OWNER TO postgres;

--
-- Name: TABLE fmis_requests; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON TABLE niva.fmis_requests IS 'LOOKUP Do not generate form.
Contains the requests by the client and the corresponding responses.';


--
-- Name: fmis_requests_contexts; Type: TABLE; Schema: niva; Owner: postgres
--

CREATE TABLE niva.fmis_requests_contexts (
    fmis_requestscontext_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    fmis_request_id integer NOT NULL,
    comment character varying,
    parcelsstatus_id integer NOT NULL,
    usrinsert character varying(150),
    dteinsert timestamp with time zone DEFAULT now(),
    usrupdate character varying(150),
    dteupdate timestamp with time zone DEFAULT now(),
    row_version integer DEFAULT 0 NOT NULL
);


ALTER TABLE niva.fmis_requests_contexts OWNER TO postgres;

--
-- Name: TABLE fmis_requests_contexts; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON TABLE niva.fmis_requests_contexts IS 'LOOKUP Do not generate form.
Contains the contexts related to FMIS requests.';


--
-- Name: fmis_uploads; Type: TABLE; Schema: niva; Owner: postgres
--

CREATE TABLE niva.fmis_uploads (
    fmisupload_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    metadata character varying,
    dteinsert timestamp with time zone DEFAULT now() NOT NULL,
    document bytea,
    usrinsert character varying(150),
    usrupdate character varying(150),
    dteupdate timestamp with time zone DEFAULT now(),
    row_version integer DEFAULT 0 NOT NULL,
    datetime timestamp with time zone DEFAULT now() NOT NULL,
    auxdecision_id integer NOT NULL
);


ALTER TABLE niva.fmis_uploads OWNER TO postgres;

--
-- Name: TABLE fmis_uploads; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON TABLE niva.fmis_uploads IS 'LOOKUP Do not generate form.
Uploaded FMIS documents.';


--
-- Name: gp_uploads; Type: TABLE; Schema: niva; Owner: postgres
--

CREATE TABLE niva.gp_uploads (
    gpupload_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    metadata character varying,
    image bytea,
    datetime timestamp with time zone DEFAULT now() NOT NULL,
    auxdecision_id integer NOT NULL,
    usrinsert character varying(150),
    dteinsert timestamp with time zone DEFAULT now(),
    usrupdate character varying(150),
    dteupdate timestamp with time zone DEFAULT now(),
    row_version integer DEFAULT 0 NOT NULL
);


ALTER TABLE niva.gp_uploads OWNER TO postgres;

--
-- Name: TABLE gp_uploads; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON TABLE niva.gp_uploads IS 'LOOKUP Do not generate form.
Contains the uploaded GP Photo files.';


--
-- Name: parcels_decisions; Type: TABLE; Schema: niva; Owner: postgres
--

CREATE TABLE niva.parcels_decisions (
    parcelsdecision_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    brerun_id integer NOT NULL,
    decision_light smallint NOT NULL,
    usrinsert character varying(150),
    dteinsert timestamp(0) with time zone DEFAULT now(),
    usrupdate character varying(150),
    dteupdate timestamp(0) with time zone DEFAULT now(),
    row_version integer DEFAULT 0 NOT NULL,
    uniqueparcel_id integer NOT NULL
);


ALTER TABLE niva.parcels_decisions OWNER TO postgres;

--
-- Name: TABLE parcels_decisions; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON TABLE niva.parcels_decisions IS 'Parcels Decisions based on BRE.';


--
-- Name: COLUMN parcels_decisions.parcelsdecision_id; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.parcels_decisions.parcelsdecision_id IS 'primary key';


--
-- Name: COLUMN parcels_decisions.decision_light; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.parcels_decisions.decision_light IS '1: green, 2: yellow, 3:red';


--
-- Name: parcels_imports; Type: TABLE; Schema: niva; Owner: postgres
--

CREATE TABLE niva.parcels_imports (
    parcelsimport_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    classifier_id integer NOT NULL,
    name character varying(60) NOT NULL,
    description character varying(150),
    usrinsert character varying(150),
    dteinsert timestamp with time zone DEFAULT now(),
    usrupdate character varying(150),
    dteupdate timestamp with time zone DEFAULT now(),
    row_version integer DEFAULT 0 NOT NULL,
    file_path character varying(60),
    attached_file bytea,
    filetemplate_id integer NOT NULL,
    year smallint DEFAULT 1970 NOT NULL,
    is_imported boolean DEFAULT false,
    is_read boolean DEFAULT false
);


ALTER TABLE niva.parcels_imports OWNER TO postgres;

--
-- Name: TABLE parcels_imports; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON TABLE niva.parcels_imports IS 'Contains the Classification & Declaration Imports data, with attached the source file.';


--
-- Name: COLUMN parcels_imports.parcelsimport_id; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.parcels_imports.parcelsimport_id IS 'primary key';


--
-- Name: COLUMN parcels_imports.classifier_id; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.parcels_imports.classifier_id IS 'fk to classifier';


--
-- Name: COLUMN parcels_imports.filetemplate_id; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.parcels_imports.filetemplate_id IS 'fk to file template';


--
-- Name: COLUMN parcels_imports.year; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.parcels_imports.year IS 'The year that the classifications is about.';


--
-- Name: COLUMN parcels_imports.is_imported; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.parcels_imports.is_imported IS 'True if the file has been parsed.';


--
-- Name: parcels_imports_data; Type: TABLE; Schema: niva; Owner: postgres
--

CREATE TABLE niva.parcels_imports_data (
    parcels_imports_data_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    decl_cultivation_id integer NOT NULL,
    pred1_cultivation_id integer NOT NULL,
    pred1_probability double precision NOT NULL,
    pred2_cultivation_id integer NOT NULL,
    pred2_probability double precision NOT NULL,
    parcelsimport_id integer NOT NULL,
    uniqueparcel_id integer NOT NULL,
    usrinsert character varying(150),
    dteinsert timestamp with time zone DEFAULT now(),
    usrupdate character varying(150),
    dteupdate timestamp with time zone DEFAULT now(),
    row_version integer DEFAULT 0 NOT NULL
);


ALTER TABLE niva.parcels_imports_data OWNER TO postgres;

--
-- Name: TABLE parcels_imports_data; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON TABLE niva.parcels_imports_data IS 'Contains The Data of the Parcels per Import.';


--
-- Name: parcels_readdata; Type: TABLE; Schema: niva; Owner: postgres
--

CREATE TABLE niva.parcels_readdata (
    identifier character varying NOT NULL,
    parcel_code character varying NOT NULL,
    geom4326 public.geometry(MultiPolygon,4326) NOT NULL,
    parcelsreaddata_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    decl_cultivation_code integer NOT NULL,
    pred1_cultivation_code integer NOT NULL,
    pred1_probability double precision NOT NULL,
    pred2_cultivation_code integer NOT NULL,
    pred2_probability double precision NOT NULL,
    parcelsimport_id integer NOT NULL,
    producer_code character varying NOT NULL,
    roworder integer NOT NULL,
    hash character(32) NOT NULL,
    usrinsert character varying(150),
    dteinsert timestamp with time zone DEFAULT now(),
    usrupdate character varying(150),
    dteupdate timestamp with time zone DEFAULT now(),
    row_version integer DEFAULT 0 NOT NULL
);


ALTER TABLE niva.parcels_readdata OWNER TO postgres;

--
-- Name: TABLE parcels_readdata; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON TABLE niva.parcels_readdata IS 'Contains Data of the parcels to be imported.';


--
-- Name: parcels_readdata_errors; Type: TABLE; Schema: niva; Owner: postgres
--

CREATE TABLE niva.parcels_readdata_errors (
    parcelsreaddataerror_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    errorcode character varying(5) NOT NULL,
    identifier character varying NOT NULL,
    parcelsimport_id integer,
    usrinsert character varying(150),
    dteinsert timestamp with time zone DEFAULT now(),
    usrupdate character varying(150),
    dteupdate timestamp with time zone DEFAULT now(),
    row_version integer DEFAULT 0 NOT NULL
);


ALTER TABLE niva.parcels_readdata_errors OWNER TO postgres;

--
-- Name: COLUMN parcels_readdata_errors.errorcode; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.parcels_readdata_errors.errorcode IS 'd0cc1= Declared Cultivation Code Uknown.
p1cc1= Predicted 1 Cultivation Code Uknown.
p2cc1= Predicted 2 Cultivation Code Uknown.
pric1= Producer''s Code Uknown.
dplid=Duplicate Identifier.';


--
-- Name: parcels_status; Type: TABLE; Schema: niva; Owner: postgres
--

CREATE TABLE niva.parcels_status (
    parcelsstatus_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    trafficlight smallint NOT NULL,
    origin smallint NOT NULL,
    origin_id integer NOT NULL,
    lockedlight smallint NOT NULL,
    paymentsscheme_id integer NOT NULL,
    usrinsert character varying(150),
    dteinsert timestamp with time zone DEFAULT now(),
    usrupdate character varying(150),
    dteupdate timestamp with time zone DEFAULT now(),
    row_version integer DEFAULT 0 NOT NULL,
    datetime timestamp with time zone DEFAULT now() NOT NULL,
    uniqueparcel_id integer NOT NULL
);


ALTER TABLE niva.parcels_status OWNER TO postgres;

--
-- Name: TABLE parcels_status; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON TABLE niva.parcels_status IS 'Contains the status items of the parcels.';


--
-- Name: COLUMN parcels_status.origin; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.parcels_status.origin IS 'The source of the status.
1=BRE Run
2=Aux Decision
4=Lock';


--
-- Name: parcels_status_details; Type: VIEW; Schema: niva; Owner: postgres
--

CREATE VIEW niva.parcels_status_details AS
 SELECT ps.parcelsstatus_id AS view_key,
    ps.parcelsstatus_id,
    ps.trafficlight,
    ps.origin,
    ps.origin_id AS brerun_id,
    ps.paymentsscheme_id,
    ps.lockedlight,
    ps.datetime,
    ps.uniqueparcel_id,
    ps.uniqueparcel_id AS vuniqueparcel_id
   FROM niva.parcels_status ps;


ALTER TABLE niva.parcels_status_details OWNER TO postgres;

--
-- Name: unique_parcels; Type: TABLE; Schema: niva; Owner: postgres
--

CREATE TABLE niva.unique_parcels (
    uniqueparcel_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    identifier character varying NOT NULL,
    year smallint NOT NULL,
    hash character(32) NOT NULL,
    usrinsert character varying(150),
    dteinsert timestamp with time zone DEFAULT now(),
    usrupdate character varying(150),
    dteupdate timestamp with time zone DEFAULT now(),
    row_version integer DEFAULT 0 NOT NULL,
    geom4326 public.geometry(Geometry,4326),
    parcel_code character varying NOT NULL,
    producer_code character varying
);


ALTER TABLE niva.unique_parcels OWNER TO postgres;

--
-- Name: TABLE unique_parcels; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON TABLE niva.unique_parcels IS 'Contains Unique parcels per Identifier & Year.';


--
-- Name: parcels_status_latest; Type: VIEW; Schema: niva; Owner: postgres
--

CREATE VIEW niva.parcels_status_latest AS
 SELECT DISTINCT ON (ps.paymentsscheme_id, ps.uniqueparcel_id) ps.parcelsstatus_id AS view_key,
    up.identifier,
    up.parcel_code,
    up.producer_code,
    ps.uniqueparcel_id,
    ps.uniqueparcel_id AS vuniqueparcel_id,
    ps.parcelsstatus_id,
    ps.trafficlight,
    ps.origin,
    ps.origin_id,
    ps.paymentsscheme_id,
    ps.lockedlight,
    ps.datetime,
    ( SELECT (EXISTS ( SELECT 1
                   FROM (niva.gp_uploads gpu
                     JOIN niva.aux_decisions ad ON ((ad.auxdecision_id = gpu.auxdecision_id)))
                  WHERE (ad.uniqueparcel_id = ps.uniqueparcel_id)
                 LIMIT 1)) AS "exists") AS gpexist,
    ( SELECT (EXISTS ( SELECT 1
                   FROM (niva.fmis_uploads fu
                     JOIN niva.aux_decisions ad ON ((ad.auxdecision_id = fu.auxdecision_id)))
                  WHERE (ad.uniqueparcel_id = ps.uniqueparcel_id)
                 LIMIT 1)) AS "exists") AS fdexist
   FROM (niva.parcels_status ps
     JOIN niva.unique_parcels up ON ((ps.uniqueparcel_id = up.uniqueparcel_id)))
  ORDER BY ps.paymentsscheme_id, ps.uniqueparcel_id, ps.datetime DESC;


ALTER TABLE niva.parcels_status_latest OWNER TO postgres;

--
-- Name: VIEW parcels_status_latest; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON VIEW niva.parcels_status_latest IS 'Displays the latest status record for each parcel.';


--
-- Name: paymentsschemes; Type: TABLE; Schema: niva; Owner: postgres
--

CREATE TABLE niva.paymentsschemes (
    paymentsscheme_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    year smallint NOT NULL,
    evaluationmethod smallint NOT NULL,
    usrinsert character varying(150),
    dteinsert timestamp with time zone DEFAULT now(),
    usrupdate character varying(150),
    dteupdate time without time zone DEFAULT now(),
    row_version integer DEFAULT 0 NOT NULL,
    name character varying(60) NOT NULL,
    description character varying(150) NOT NULL
);


ALTER TABLE niva.paymentsschemes OWNER TO postgres;

--
-- Name: TABLE paymentsschemes; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON TABLE niva.paymentsschemes IS 'Contains the BRE Runs chains.';


--
-- Name: producers; Type: TABLE; Schema: niva; Owner: postgres
--

CREATE TABLE niva.producers (
    firstname character varying(40),
    row_version integer DEFAULT 0 NOT NULL,
    producer_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    usrinsert character varying(150),
    dteinsert timestamp with time zone DEFAULT now(),
    usrupdate character varying(150),
    dteupdate timestamp with time zone DEFAULT now(),
    code character varying NOT NULL,
    lastname character varying(40) NOT NULL
);


ALTER TABLE niva.producers OWNER TO postgres;

--
-- Name: TABLE producers; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON TABLE niva.producers IS 'Contains info about the farmers.';


--
-- Name: system_configurations; Type: TABLE; Schema: niva; Owner: postgres
--

CREATE TABLE niva.system_configurations (
    srid smallint NOT NULL,
    language character varying(2) NOT NULL,
    usrinsert character varying(150),
    dteinsert timestamp with time zone DEFAULT now(),
    usrupdate character varying(150),
    dteupdate timestamp with time zone DEFAULT now(),
    row_version integer DEFAULT 0 NOT NULL,
    systemconfiguration_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    name character varying(60) NOT NULL,
    description character varying(150) NOT NULL,
    agency character varying(60) NOT NULL,
    tmpdir character varying(250) NOT NULL,
    documentationmaxfilesize integer DEFAULT 1024 NOT NULL,
    active boolean DEFAULT false NOT NULL,
    maxcsvrecords integer DEFAULT 1000000
);


ALTER TABLE niva.system_configurations OWNER TO postgres;

--
-- Name: TABLE system_configurations; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON TABLE niva.system_configurations IS 'Contains parameters about the configuration.';


--
-- Name: COLUMN system_configurations.active; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.system_configurations.active IS 'True if this is the current active profile.';


--
-- Name: COLUMN system_configurations.maxcsvrecords; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON COLUMN niva.system_configurations.maxcsvrecords IS 'The maximum number of rows that can be exported to a CSV file.';


--
-- Name: tmp_blob_id_seq; Type: SEQUENCE; Schema: niva; Owner: postgres
--

CREATE SEQUENCE niva.tmp_blob_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE niva.tmp_blob_id_seq OWNER TO postgres;

--
-- Name: tmp_blob; Type: TABLE; Schema: niva; Owner: postgres
--

CREATE TABLE niva.tmp_blob (
    id integer DEFAULT nextval('niva.tmp_blob_id_seq'::regclass) NOT NULL,
    data bytea,
    row_version integer DEFAULT 0 NOT NULL,
    dteinsert timestamp with time zone DEFAULT now(),
    usrinsert character varying(150),
    dteupdate timestamp with time zone DEFAULT now(),
    usrupdate character varying(150)
);


ALTER TABLE niva.tmp_blob OWNER TO postgres;

--
-- Name: user_notifications; Type: TABLE; Schema: niva; Owner: postgres
--

CREATE TABLE niva.user_notifications (
    user_notification_id integer DEFAULT nextval('niva.niva_sq'::regclass) NOT NULL,
    type smallint DEFAULT 0 NOT NULL,
    usrinsert character varying(150),
    dteinsert timestamp with time zone DEFAULT now(),
    usrupdate character varying(150),
    dteupdate timestamp with time zone DEFAULT now(),
    row_version integer DEFAULT 0 NOT NULL
);


ALTER TABLE niva.user_notifications OWNER TO postgres;

--
-- Name: TABLE user_notifications; Type: COMMENT; Schema: niva; Owner: postgres
--

COMMENT ON TABLE niva.user_notifications IS 'Contains Notifications about the DSS.';


--
-- Name: parcels_classdecldata; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.parcels_classdecldata (
    pcla_id integer,
    clas_id integer,
    cult_id_decl integer,
    cult_id_pred integer,
    coty_id_decl integer,
    coty_id_pred integer,
    prob_pred double precision,
    cult_id_pred2 integer,
    prob_pred2 double precision,
    prod_code integer,
    parc_identifier character varying,
    parc_code character varying,
    geom4326 public.geometry(MultiPolygon,4326),
    usrinsert character varying(150),
    dteinsert timestamp with time zone,
    usrupdate character varying(150),
    dteupdate timestamp with time zone,
    row_version integer
);

--
-- View: niva.declared_cultivation_per_paymentsscheme
--

CREATE VIEW niva.declared_cultivation_per_paymentsscheme AS
 WITH declcultsub AS (
         SELECT pid.uniqueparcel_id,
            pid.decl_cultivation_id,
            br.brerun_id
           FROM ((niva.parcels_imports_data pid
             JOIN niva.parcels_imports pi ON ((pi.parcelsimport_id = pid.parcelsimport_id)))
             JOIN niva.breruns br ON ((br.parcelsimport_id = pi.parcelsimport_id)))
        ), pssub AS (
         SELECT DISTINCT ON (ps.uniqueparcel_id, ps.paymentsscheme_id) ps.uniqueparcel_id,
            ps.paymentsscheme_id,
            ps.origin_id
           FROM niva.parcels_status ps
          WHERE (ps.origin_id IS NOT NULL)
          ORDER BY ps.uniqueparcel_id, ps.paymentsscheme_id, ps.datetime DESC
        )
 SELECT pssub.paymentsscheme_id AS view_key,
    ad.auxdecision_id,
    pssub.uniqueparcel_id,
    pssub.paymentsscheme_id,
    declcultsub.decl_cultivation_id
   FROM (niva.aux_decisions ad
     JOIN pssub ON ((pssub.uniqueparcel_id = ad.uniqueparcel_id))),
    declcultsub
  WHERE ((declcultsub.brerun_id = pssub.origin_id) AND (declcultsub.uniqueparcel_id = pssub.uniqueparcel_id));

--
-- View: niva.photos_on_map
--

CREATE VIEW niva.photos_on_map AS
 WITH xydir AS (
         SELECT gp_uploads_1.gpupload_id,
            ((gp_uploads_1.metadata::json -> 'longitude'::text)::text)::double precision AS x,
            ((gp_uploads_1.metadata::json -> 'latitude'::text)::text)::double precision AS y,
            split_part((gp_uploads_1.metadata::json -> 'direction'::text)::text, ' '::text, 3)::double precision AS dir
           FROM niva.gp_uploads gp_uploads_1
        ), parameters(radius, quadsegs) AS (
         VALUES (0.00002,2)
        )
 SELECT gp_uploads.gpupload_id AS view_key,
    gp_uploads.gpupload_id,
    gp_uploads.gpupload_id AS vgpupload_id,
    gp_uploads.auxdecision_id,
    gp_uploads.metadata,
    gp_uploads.datetime AS datetimeuploaded,
        CASE
            WHEN xydir.x IS NOT NULL AND xydir.y IS NOT NULL THEN
            CASE
                WHEN xydir.dir IS NOT NULL THEN public.st_polygon(public.st_geomfromtext(concat('LINESTRING(', (xydir.x + 0.0001::double precision * sin((xydir.dir - 12::double precision) * pi() / 180::double precision))::text, ' '::text, (xydir.y + 0.0001::double precision * cos((xydir.dir - 12::double precision) * pi() / 180::double precision))::text, ', '::text, xydir.x::text, ' '::text, xydir.y::text, ', '::text, (xydir.x + 0.0001::double precision * sin((xydir.dir + 12::double precision) * pi() / 180::double precision))::text, ' '::text, (xydir.y + 0.0001::double precision * cos((xydir.dir + 12::double precision) * pi() / 180::double precision))::text, ', '::text, (xydir.x + 0.0001::double precision * sin((xydir.dir - 12::double precision) * pi() / 180::double precision))::text, ' '::text, (xydir.y + 0.0001::double precision * cos((xydir.dir - 12::double precision) * pi() / 180::double precision))::text, ')'::text), 4326), 4326)
                ELSE public.st_buffer(public.st_setsrid(public.st_point(xydir.x, xydir.y), 4326), parameters.radius::double precision, parameters.quadsegs)
            END
            ELSE NULL::public.geometry
        END AS geom4326
   FROM niva.gp_uploads
     JOIN xydir ON gp_uploads.gpupload_id = xydir.gpupload_id,
    parameters;


ALTER TABLE niva.declared_cultivation_per_paymentsscheme OWNER TO postgres;

ALTER TABLE public.parcels_classdecldata OWNER TO postgres;

--
-- Name: aux_decisions ad_uniqueparcels_id_un; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.aux_decisions
    ADD CONSTRAINT ad_uniqueparcels_id_un UNIQUE (uniqueparcel_id);


--
-- Name: agrisnap_requests_contexts agrisnap_requests_contexts_pkey; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.agrisnap_requests_contexts
    ADD CONSTRAINT agrisnap_requests_contexts_pkey PRIMARY KEY (agrisnap_requestscontext_id);


--
-- Name: agrisnap_requests agrisnaprequests_pkey; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.agrisnap_requests
    ADD CONSTRAINT agrisnaprequests_pkey PRIMARY KEY (agrisnaprequest_id);


--
-- Name: agrisnap_users agrisnapusers_pkey; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.agrisnap_users
    ADD CONSTRAINT agrisnapusers_pkey PRIMARY KEY (agrisnapuser_id);


--
-- Name: agrisnap_users agrisnapusers_uid_un; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.agrisnap_users
    ADD CONSTRAINT agrisnapusers_uid_un UNIQUE (agrisnap_uid);


--
-- Name: agrisnap_usersproducers agrisnapusersproducers_pkey; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.agrisnap_usersproducers
    ADD CONSTRAINT agrisnapusersproducers_pkey PRIMARY KEY (agrisnapusersproducer_id);


--
-- Name: aux_decisions aux_decisions_pkey; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.aux_decisions
    ADD CONSTRAINT aux_decisions_pkey PRIMARY KEY (auxdecision_id);


--
-- Name: breruns breruns_pkey; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.breruns
    ADD CONSTRAINT breruns_pkey PRIMARY KEY (brerun_id);


--
-- Name: classifiers classifiers_pkey; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.classifiers
    ADD CONSTRAINT classifiers_pkey PRIMARY KEY (classifier_id);


--
-- Name: producers code_un; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.producers
    ADD CONSTRAINT code_un UNIQUE (code);


--
-- Name: cover_types cover_types_code_un; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.cover_types
    ADD CONSTRAINT cover_types_code_un UNIQUE (code);


--
-- Name: cover_types cover_types_codename_un; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.cover_types
    ADD CONSTRAINT cover_types_codename_un UNIQUE (code, name);


--
-- Name: cover_types cover_types_name_un; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.cover_types
    ADD CONSTRAINT cover_types_name_un UNIQUE (name);


--
-- Name: cover_types covertypes_pkey; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.cover_types
    ADD CONSTRAINT covertypes_pkey PRIMARY KEY (covertype_id);


--
-- Name: cultivations cultivations_code_un; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.cultivations
    ADD CONSTRAINT cultivations_code_un UNIQUE (code);


--
-- Name: cultivations cultivations_codename_un; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.cultivations
    ADD CONSTRAINT cultivations_codename_un UNIQUE (name, code);


--
-- Name: cultivations cultivations_name_un; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.cultivations
    ADD CONSTRAINT cultivations_name_un UNIQUE (name);


--
-- Name: cultivations cultivations_pkey; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.cultivations
    ADD CONSTRAINT cultivations_pkey PRIMARY KEY (cultivation_id);


--
-- Name: dbmanagementlogs dbmanagementlogs_pkey; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.dbmanagementlogs
    ADD CONSTRAINT dbmanagementlogs_pkey PRIMARY KEY (dbml_id);


--
-- Name: documents documents_description_un; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.documents
    ADD CONSTRAINT documents_description_un UNIQUE (description);


--
-- Name: documents documents_pkey; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.documents
    ADD CONSTRAINT documents_pkey PRIMARY KEY (document_id);


--
-- Name: ec_sets ecsets_pkey; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.ec_sets
    ADD CONSTRAINT ecsets_pkey PRIMARY KEY (ecset_id);


--
-- Name: ec_sets_cslcs ecsetscslcs_pkey; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.ec_sets_cslcs
    ADD CONSTRAINT ecsetscslcs_pkey PRIMARY KEY (ecsetsclc_id);


--
-- Name: ec_sets_cslcs_details ecsetscslcsdetail_ecsetsclc_id; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.ec_sets_cslcs_details
    ADD CONSTRAINT ecsetscslcsdetail_ecsetsclc_id UNIQUE (ecsetsclc_id, ordering_number);


--
-- Name: ec_sets_cslcs_details ecsetscslcsdetails_pkey; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.ec_sets_cslcs_details
    ADD CONSTRAINT ecsetscslcsdetails_pkey PRIMARY KEY (ecsetscslcsdetail_id);


--
-- Name: excel_errors excelerrors_pkey; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.excel_errors
    ADD CONSTRAINT excelerrors_pkey PRIMARY KEY (id);


--
-- Name: excel_files excelfiles_pkey; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.excel_files
    ADD CONSTRAINT excelfiles_pkey PRIMARY KEY (id);


--
-- Name: file_templates file_template_name_un; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.file_templates
    ADD CONSTRAINT file_template_name_un UNIQUE (name);


--
-- Name: file_templates filetemplates_pkey; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.file_templates
    ADD CONSTRAINT filetemplates_pkey PRIMARY KEY (filetemplate_id);


--
-- Name: fmis_requests_contexts fmis_requests_contexts_pkey; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.fmis_requests_contexts
    ADD CONSTRAINT fmis_requests_contexts_pkey PRIMARY KEY (fmis_requestscontext_id);


--
-- Name: fmis_uploads fmis_uploads_pkey; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.fmis_uploads
    ADD CONSTRAINT fmis_uploads_pkey PRIMARY KEY (fmisupload_id);


--
-- Name: fmis_requests fmisrequests_pkey; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.fmis_requests
    ADD CONSTRAINT fmisrequests_pkey PRIMARY KEY (fmisrequest_id);


--
-- Name: gp_uploads gp_uploads_pkey; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.gp_uploads
    ADD CONSTRAINT gp_uploads_pkey PRIMARY KEY (gpupload_id);


--
-- Name: tmp_blob nitb_pk; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.tmp_blob
    ADD CONSTRAINT nitb_pk PRIMARY KEY (id);


--
-- Name: parcels_readdata_errors parcels_readdata_errors_pkey; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.parcels_readdata_errors
    ADD CONSTRAINT parcels_readdata_errors_pkey PRIMARY KEY (parcelsreaddataerror_id);


--
-- Name: parcels_readdata parcels_readdata_pkey; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.parcels_readdata
    ADD CONSTRAINT parcels_readdata_pkey PRIMARY KEY (parcelsreaddata_id);


--
-- Name: parcels_decisions parcelsdecisions_pkey; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.parcels_decisions
    ADD CONSTRAINT parcelsdecisions_pkey PRIMARY KEY (parcelsdecision_id);


--
-- Name: parcels_imports parcelsimports_pkey; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.parcels_imports
    ADD CONSTRAINT parcelsimports_pkey PRIMARY KEY (parcelsimport_id);


--
-- Name: parcels_imports_data parcelsimportsdata_pkey; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.parcels_imports_data
    ADD CONSTRAINT parcelsimportsdata_pkey PRIMARY KEY (parcels_imports_data_id);


--
-- Name: parcels_status parcelsstatus_pkey; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.parcels_status
    ADD CONSTRAINT parcelsstatus_pkey PRIMARY KEY (parcelsstatus_id);


--
-- Name: paymentsschemes paymentsschemes_pkey; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.paymentsschemes
    ADD CONSTRAINT paymentsschemes_pkey PRIMARY KEY (paymentsscheme_id);


--
-- Name: producers producers_pkey; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.producers
    ADD CONSTRAINT producers_pkey PRIMARY KEY (producer_id);


--
-- Name: system_configurations system_configurations_pkey; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.system_configurations
    ADD CONSTRAINT system_configurations_pkey PRIMARY KEY (systemconfiguration_id);


--
-- Name: system_configurations systemconfigurations_name_un; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.system_configurations
    ADD CONSTRAINT systemconfigurations_name_un UNIQUE (name);


--
-- Name: unique_parcels unique_parcels_pkey; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.unique_parcels
    ADD CONSTRAINT unique_parcels_pkey PRIMARY KEY (uniqueparcel_id);


--
-- Name: unique_parcels uniqueparcels_hash_un; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.unique_parcels
    ADD CONSTRAINT uniqueparcels_hash_un UNIQUE (hash);


--
-- Name: user_notifications usernotifications_pkey; Type: CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.user_notifications
    ADD CONSTRAINT usernotifications_pkey PRIMARY KEY (user_notification_id);


--
-- Name: exer_exfi_id_fi; Type: INDEX; Schema: niva; Owner: postgres
--

CREATE INDEX exer_exfi_id_fi ON niva.excel_errors USING btree (exfi_id);


--
-- Name: indx_ar_1; Type: INDEX; Schema: niva; Owner: postgres
--

CREATE INDEX indx_ar_1 ON niva.agrisnap_requests USING btree (agrisnap_uid);


--
-- Name: indx_arc_1; Type: INDEX; Schema: niva; Owner: postgres
--

CREATE INDEX indx_arc_1 ON niva.agrisnap_requests_contexts USING btree (agrisnap_request_id);


--
-- Name: indx_au_1; Type: INDEX; Schema: niva; Owner: postgres
--

CREATE INDEX indx_au_1 ON niva.agrisnap_users USING btree (agrisnap_uid);


--
-- Name: indx_aup_1; Type: INDEX; Schema: niva; Owner: postgres
--

CREATE INDEX indx_aup_1 ON niva.agrisnap_usersproducers USING btree (agrisnapuser_id, producer_id);


--
-- Name: indx_breruns_1; Type: INDEX; Schema: niva; Owner: postgres
--

CREATE INDEX indx_breruns_1 ON niva.breruns USING btree (parcelsimport_id);


--
-- Name: indx_ct_1; Type: INDEX; Schema: niva; Owner: postgres
--

CREATE INDEX indx_ct_1 ON niva.cover_types USING btree (code);


--
-- Name: indx_cult_1; Type: INDEX; Schema: niva; Owner: postgres
--

CREATE INDEX indx_cult_1 ON niva.cultivations USING btree (code);


--
-- Name: indx_cult_2; Type: INDEX; Schema: niva; Owner: postgres
--

CREATE INDEX indx_cult_2 ON niva.cultivations USING btree (covertype_id);


--
-- Name: indx_dbml_1; Type: INDEX; Schema: niva; Owner: postgres
--

CREATE INDEX indx_dbml_1 ON niva.dbmanagementlogs USING btree (dte_done DESC NULLS LAST);


--
-- Name: indx_decisions_1; Type: INDEX; Schema: niva; Owner: postgres
--

CREATE INDEX indx_decisions_1 ON niva.aux_decisions USING btree (uniqueparcel_id);


--
-- Name: indx_ec_sets_csics_1; Type: INDEX; Schema: niva; Owner: postgres
--

CREATE INDEX indx_ec_sets_csics_1 ON niva.ec_sets_cslcs USING btree (ecset_id);


--
-- Name: indx_ec_sets_cslcs_details_1; Type: INDEX; Schema: niva; Owner: postgres
--

CREATE INDEX indx_ec_sets_cslcs_details_1 ON niva.ec_sets_cslcs_details USING btree (ecsetsclc_id, ordering_number);


--
-- Name: indx_ecsets_1; Type: INDEX; Schema: niva; Owner: postgres
--

CREATE INDEX indx_ecsets_1 ON niva.ec_sets USING btree (evaluationmethod);


--
-- Name: indx_fmis_requests_1; Type: INDEX; Schema: niva; Owner: postgres
--

CREATE INDEX indx_fmis_requests_1 ON niva.fmis_requests USING btree (producer_id, datetime DESC NULLS LAST);


--
-- Name: indx_fmis_requests_contexts_1; Type: INDEX; Schema: niva; Owner: postgres
--

CREATE INDEX indx_fmis_requests_contexts_1 ON niva.fmis_requests_contexts USING btree (fmis_request_id);


--
-- Name: indx_fu_1; Type: INDEX; Schema: niva; Owner: postgres
--

CREATE INDEX indx_fu_1 ON niva.fmis_uploads USING btree (auxdecision_id);


--
-- Name: indx_gp_uploads_1; Type: INDEX; Schema: niva; Owner: postgres
--

CREATE INDEX indx_gp_uploads_1 ON niva.gp_uploads USING btree (auxdecision_id);


--
-- Name: indx_paymentsschemes_1; Type: INDEX; Schema: niva; Owner: postgres
--

CREATE INDEX indx_paymentsschemes_1 ON niva.paymentsschemes USING btree (evaluationmethod, year DESC NULLS LAST);


--
-- Name: indx_pd_1; Type: INDEX; Schema: niva; Owner: postgres
--

CREATE INDEX indx_pd_1 ON niva.parcels_decisions USING btree (brerun_id, uniqueparcel_id);


--
-- Name: indx_pd_2; Type: INDEX; Schema: niva; Owner: postgres
--

CREATE INDEX indx_pd_2 ON niva.parcels_decisions USING btree (brerun_id, decision_light);


--
-- Name: indx_pi_1; Type: INDEX; Schema: niva; Owner: postgres
--

CREATE INDEX indx_pi_1 ON niva.parcels_imports USING btree (year DESC NULLS LAST);


--
-- Name: indx_pid_1; Type: INDEX; Schema: niva; Owner: postgres
--

CREATE INDEX indx_pid_1 ON niva.parcels_imports_data USING btree (parcelsimport_id, uniqueparcel_id);


--
-- Name: indx_prd; Type: INDEX; Schema: niva; Owner: postgres
--

CREATE INDEX indx_prd ON niva.parcels_readdata USING btree (parcelsimport_id);


--
-- Name: indx_prde_1; Type: INDEX; Schema: niva; Owner: postgres
--

CREATE INDEX indx_prde_1 ON niva.parcels_readdata_errors USING btree (parcelsimport_id);


--
-- Name: indx_producers_1; Type: INDEX; Schema: niva; Owner: postgres
--

CREATE INDEX indx_producers_1 ON niva.producers USING btree (code);


--
-- Name: indx_ps_1; Type: INDEX; Schema: niva; Owner: postgres
--

CREATE INDEX indx_ps_1 ON niva.parcels_status USING btree (paymentsscheme_id, uniqueparcel_id, datetime DESC NULLS LAST);


--
-- Name: indx_up_1; Type: INDEX; Schema: niva; Owner: postgres
--

CREATE INDEX indx_up_1 ON niva.unique_parcels USING btree (identifier, year);


--
-- Name: indx_up_2; Type: INDEX; Schema: niva; Owner: postgres
--

CREATE INDEX indx_up_2 ON niva.unique_parcels USING btree (hash);


--
-- Name: indx_up_3; Type: INDEX; Schema: niva; Owner: postgres
--

CREATE INDEX indx_up_3 ON niva.unique_parcels USING btree (producer_code);


--
-- Name: indx_up_4; Type: INDEX; Schema: niva; Owner: postgres
--

CREATE INDEX indx_up_4 ON niva.unique_parcels USING btree (year);


--
-- Name: aux_decisions ad_l1d_cultivation_id; Type: FK CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.aux_decisions
    ADD CONSTRAINT ad_l1d_cultivation_id FOREIGN KEY (level1decision) REFERENCES niva.cultivations(cultivation_id) NOT VALID;


--
-- Name: aux_decisions ad_l2d_covertype_id; Type: FK CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.aux_decisions
    ADD CONSTRAINT ad_l2d_covertype_id FOREIGN KEY (level2decision) REFERENCES niva.cover_types(covertype_id) NOT VALID;


--
-- Name: aux_decisions ad_up_uniqueparcel_id; Type: FK CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.aux_decisions
    ADD CONSTRAINT ad_up_uniqueparcel_id FOREIGN KEY (uniqueparcel_id) REFERENCES niva.unique_parcels(uniqueparcel_id) NOT VALID;


--
-- Name: agrisnap_requests_contexts agrisnap_rc_parcelsstatus_parcelsstatus_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.agrisnap_requests_contexts
    ADD CONSTRAINT agrisnap_rc_parcelsstatus_parcelsstatus_id_fk FOREIGN KEY (parcelsstatus_id) REFERENCES niva.parcels_status(parcelsstatus_id) NOT VALID;


--
-- Name: agrisnap_requests_contexts agrisnap_rc_r_requests_context_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.agrisnap_requests_contexts
    ADD CONSTRAINT agrisnap_rc_r_requests_context_id_fk FOREIGN KEY (agrisnap_request_id) REFERENCES niva.agrisnap_requests(agrisnaprequest_id) NOT VALID;


--
-- Name: agrisnap_usersproducers aup_au_agrisnapuser_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.agrisnap_usersproducers
    ADD CONSTRAINT aup_au_agrisnapuser_id_fk FOREIGN KEY (agrisnapuser_id) REFERENCES niva.agrisnap_users(agrisnapuser_id) NOT VALID;


--
-- Name: agrisnap_usersproducers aup_producers_producer_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.agrisnap_usersproducers
    ADD CONSTRAINT aup_producers_producer_id_fk FOREIGN KEY (producer_id) REFERENCES niva.producers(producer_id) NOT VALID;


--
-- Name: breruns breruns_ecsets_ecset_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.breruns
    ADD CONSTRAINT breruns_ecsets_ecset_id_fk FOREIGN KEY (ecset_id) REFERENCES niva.ec_sets(ecset_id);


--
-- Name: breruns breruns_parcelsimport_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.breruns
    ADD CONSTRAINT breruns_parcelsimport_id_fk FOREIGN KEY (parcelsimport_id) REFERENCES niva.parcels_imports(parcelsimport_id);


--
-- Name: cover_types covertypes_excelfiles_exfi_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.cover_types
    ADD CONSTRAINT covertypes_excelfiles_exfi_id_fk FOREIGN KEY (excelfile_id) REFERENCES niva.excel_files(id);


--
-- Name: cultivations cultivations_covertypes_covertype_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.cultivations
    ADD CONSTRAINT cultivations_covertypes_covertype_id_fk FOREIGN KEY (covertype_id) REFERENCES niva.cover_types(covertype_id);


--
-- Name: cultivations cultivations_excelfiles_exfi_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.cultivations
    ADD CONSTRAINT cultivations_excelfiles_exfi_id_fk FOREIGN KEY (excelfile_id) REFERENCES niva.excel_files(id);


--
-- Name: ec_sets_cslcs ecsetscslcs_covertypes_covertype_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.ec_sets_cslcs
    ADD CONSTRAINT ecsetscslcs_covertypes_covertype_id_fk FOREIGN KEY (covertype_id) REFERENCES niva.cover_types(covertype_id);


--
-- Name: ec_sets_cslcs ecsetscslcs_cultivations_cultivation_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.ec_sets_cslcs
    ADD CONSTRAINT ecsetscslcs_cultivations_cultivation_id_fk FOREIGN KEY (cultivation_id) REFERENCES niva.cultivations(cultivation_id);


--
-- Name: ec_sets_cslcs ecsetscslcs_ecsets_ecset_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.ec_sets_cslcs
    ADD CONSTRAINT ecsetscslcs_ecsets_ecset_id_fk FOREIGN KEY (ecset_id) REFERENCES niva.ec_sets(ecset_id);


--
-- Name: ec_sets_cslcs_details ecsetscslcsdetails_ecsetscslcs_ecsetsclc_id; Type: FK CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.ec_sets_cslcs_details
    ADD CONSTRAINT ecsetscslcsdetails_ecsetscslcs_ecsetsclc_id FOREIGN KEY (ecsetsclc_id) REFERENCES niva.ec_sets_cslcs(ecsetsclc_id) NOT VALID;


--
-- Name: excel_errors excelerrors_excelfiles_exfi_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.excel_errors
    ADD CONSTRAINT excelerrors_excelfiles_exfi_id_fk FOREIGN KEY (exfi_id) REFERENCES niva.excel_files(id) NOT VALID;


--
-- Name: fmis_requests_contexts fmis_rc_parcelsstatus_parcelsstatus_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.fmis_requests_contexts
    ADD CONSTRAINT fmis_rc_parcelsstatus_parcelsstatus_id_fk FOREIGN KEY (parcelsstatus_id) REFERENCES niva.parcels_status(parcelsstatus_id);


--
-- Name: fmis_requests_contexts fmis_rc_r_requests_context_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.fmis_requests_contexts
    ADD CONSTRAINT fmis_rc_r_requests_context_id_fk FOREIGN KEY (fmis_request_id) REFERENCES niva.fmis_requests(fmisrequest_id);


--
-- Name: fmis_requests fmisrequests_producers_producer_id; Type: FK CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.fmis_requests
    ADD CONSTRAINT fmisrequests_producers_producer_id FOREIGN KEY (producer_id) REFERENCES niva.producers(producer_id);


--
-- Name: fmis_uploads fmisu_ad_auxdecision_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.fmis_uploads
    ADD CONSTRAINT fmisu_ad_auxdecision_id_fk FOREIGN KEY (auxdecision_id) REFERENCES niva.aux_decisions(auxdecision_id) NOT VALID;


--
-- Name: gp_uploads gpu_ad_auxdecision_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.gp_uploads
    ADD CONSTRAINT gpu_ad_auxdecision_id_fk FOREIGN KEY (auxdecision_id) REFERENCES niva.aux_decisions(auxdecision_id) NOT VALID;


--
-- Name: parcels_imports parcelsimports_classifiers_classifier_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.parcels_imports
    ADD CONSTRAINT parcelsimports_classifiers_classifier_id_fk FOREIGN KEY (classifier_id) REFERENCES niva.classifiers(classifier_id);


--
-- Name: parcels_imports parcelslimports_filetemplates_filetemplate_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.parcels_imports
    ADD CONSTRAINT parcelslimports_filetemplates_filetemplate_id_fk FOREIGN KEY (filetemplate_id) REFERENCES niva.file_templates(filetemplate_id);


--
-- Name: parcels_readdata parcelsreaddata_parcelsimports_parcels_import_id; Type: FK CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.parcels_readdata
    ADD CONSTRAINT parcelsreaddata_parcelsimports_parcels_import_id FOREIGN KEY (parcelsimport_id) REFERENCES niva.parcels_imports(parcelsimport_id) NOT VALID;


--
-- Name: parcels_status parcelsstatus_paymentsscheme_paymentscheme_id; Type: FK CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.parcels_status
    ADD CONSTRAINT parcelsstatus_paymentsscheme_paymentscheme_id FOREIGN KEY (paymentsscheme_id) REFERENCES niva.paymentsschemes(paymentsscheme_id);


--
-- Name: parcels_status parcelsstatus_up_uniqueparcel_id; Type: FK CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.parcels_status
    ADD CONSTRAINT parcelsstatus_up_uniqueparcel_id FOREIGN KEY (uniqueparcel_id) REFERENCES niva.unique_parcels(uniqueparcel_id) NOT VALID;


--
-- Name: parcels_decisions pd_breruns_brerun_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.parcels_decisions
    ADD CONSTRAINT pd_breruns_brerun_id_fk FOREIGN KEY (brerun_id) REFERENCES niva.breruns(brerun_id);


--
-- Name: parcels_decisions pd_up_uniqueparcel_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.parcels_decisions
    ADD CONSTRAINT pd_up_uniqueparcel_id_fk FOREIGN KEY (uniqueparcel_id) REFERENCES niva.unique_parcels(uniqueparcel_id) NOT VALID;


--
-- Name: parcels_imports_data pidata_cult_decl_cultivation_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.parcels_imports_data
    ADD CONSTRAINT pidata_cult_decl_cultivation_id_fk FOREIGN KEY (decl_cultivation_id) REFERENCES niva.cultivations(cultivation_id) NOT VALID;


--
-- Name: parcels_imports_data pidata_cult_pred1_cultivation_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.parcels_imports_data
    ADD CONSTRAINT pidata_cult_pred1_cultivation_id_fk FOREIGN KEY (pred1_cultivation_id) REFERENCES niva.cultivations(cultivation_id) NOT VALID;


--
-- Name: parcels_imports_data pidata_cult_pred2_cultivation_id_fk; Type: FK CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.parcels_imports_data
    ADD CONSTRAINT pidata_cult_pred2_cultivation_id_fk FOREIGN KEY (pred2_cultivation_id) REFERENCES niva.cultivations(cultivation_id) NOT VALID;


--
-- Name: parcels_imports_data pidata_pi_parcelsimport_id; Type: FK CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.parcels_imports_data
    ADD CONSTRAINT pidata_pi_parcelsimport_id FOREIGN KEY (parcelsimport_id) REFERENCES niva.parcels_imports(parcelsimport_id) NOT VALID;


--
-- Name: parcels_imports_data pidata_up_uniqueparcel_id; Type: FK CONSTRAINT; Schema: niva; Owner: postgres
--

ALTER TABLE ONLY niva.parcels_imports_data
    ADD CONSTRAINT pidata_up_uniqueparcel_id FOREIGN KEY (uniqueparcel_id) REFERENCES niva.unique_parcels(uniqueparcel_id) NOT VALID;

--
-- View: niva.declared_cultivation_per_paymentsscheme
--

ALTER TABLE niva.declared_cultivation_per_paymentsscheme OWNER TO postgres;


--
-- View: niva.photos_on_map
--

ALTER TABLE niva.photos_on_map
    OWNER TO postgres;
	

-- LOG the execution of the script (End).
INSERT INTO niva.dbmanagementlogs(
	dte_done, version, notes, type, usrinsert, dteinsert )
	VALUES (now(), 'c20221026a', '6th Release Full', '2', 'mt', now());
